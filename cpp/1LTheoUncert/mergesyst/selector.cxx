#define selector_cxx
#include "selector.h"
#include "glog/logging.h"
#include "run2ComFunc.h"
#include <vector>
#include "Cutflow.h"
#include "Utils.h"
#include "PhyUtils.h"
#include "MetaDB.h"
#include <TLorentzVector.h>
#include <algorithm>
#include <string>


std::string file;

Double_t reduceLarge(Double_t value){
	if(value < -1) return -1;
	if(value > 1) return 1;
	return value;
}

void selector::Loop()
{
   if (fChain == 0) return;

   Long64_t nentries = fChain->GetEntries();

   Long64_t nbytes = 0, nb = 0;
	//define cutflow
	Cutflow* mCutflow = new Cutflow(file);
	mCutflow->setWeight([&]{return 1;});

	/*********************************************************************
	 * ******************** Define output Tree ***************************
	 * ******************************************************************/

	string chainname = fChain->GetName();
	std::cout << "My tree name is " << chainname << std::endl;

	TTree* temp = fChain->CloneTree(0);
	temp->SetBranchStatus("*py*",0);
	TTree* m_Tree = temp->CloneTree(0);
	Double_t VR_off_Low_raup(0), VR_off_Med_raup(0), VR_off_High_raup(0), SR_h_Low_raup(0), SR_h_Med_raup(0), SR_h_High_raup(0), VR_on_Low_raup(0), VR_on_Med_raup(0), VR_on_High_raup(0);
	Double_t VR_off_Low_radw(0), VR_off_Med_radw(0), VR_off_High_radw(0), SR_h_Low_radw(0), SR_h_Med_radw(0), SR_h_High_radw(0), VR_on_Low_radw(0), VR_on_Med_radw(0), VR_on_High_radw(0);
	m_Tree->Branch("VR_off_Low_radw", &VR_off_Low_radw);
	m_Tree->Branch("VR_off_Med_radw", &VR_off_Med_radw);
	m_Tree->Branch("VR_off_High_radw", &VR_off_High_radw);
	m_Tree->Branch("SR_h_Low_radw", &SR_h_Low_radw);
	m_Tree->Branch("SR_h_Med_radw", &SR_h_Med_radw);
	m_Tree->Branch("SR_h_High_radw", &SR_h_High_radw);
	m_Tree->Branch("VR_on_Low_radw", &VR_on_Low_radw);
	m_Tree->Branch("VR_on_Med_radw", &VR_on_Med_radw);
	m_Tree->Branch("VR_on_High_radw", &VR_on_High_radw);
	m_Tree->Branch("VR_off_Low_raup", &VR_off_Low_raup);
	m_Tree->Branch("VR_off_Med_raup", &VR_off_Med_raup);
	m_Tree->Branch("VR_off_High_raup", &VR_off_High_raup);
	m_Tree->Branch("SR_h_Low_raup", &SR_h_Low_raup);
	m_Tree->Branch("SR_h_Med_raup", &SR_h_Med_raup);
	m_Tree->Branch("SR_h_High_raup", &SR_h_High_raup);
	m_Tree->Branch("VR_on_Low_raup", &VR_on_Low_raup);
	m_Tree->Branch("VR_on_Med_raup", &VR_on_Med_raup);
	m_Tree->Branch("VR_on_High_raup", &VR_on_High_raup);

	mCutflow->setFillTree(m_Tree);

	auto lastCut = mCutflow->registerCut("the END", [&] {return true; });

   for (Long64_t jentry=0; jentry<nentries;jentry++) {
      Long64_t ientry = LoadTree(jentry);
      if (ientry < 0) break;
      nb = fChain->GetEntry(jentry);   nbytes += nb;
      if (0 == jentry%100000)
      	cout << jentry << " entry of " << nentries << "entries" << endl;

		VR_off_Low_radw = -sqrt(VR_off_Low_py1dw * VR_off_Low_py1dw + VR_off_Low_py2dw * VR_off_Low_py2dw + VR_off_Low_py3adw * VR_off_Low_py3adw + VR_off_Low_py3bdw * VR_off_Low_py3bdw + VR_off_Low_py3cdw * VR_off_Low_py3cdw);
		VR_off_Med_radw = -sqrt(VR_off_Med_py1dw * VR_off_Med_py1dw + VR_off_Med_py2dw * VR_off_Med_py2dw + VR_off_Med_py3adw * VR_off_Med_py3adw + VR_off_Med_py3bdw * VR_off_Med_py3bdw + VR_off_Med_py3cdw * VR_off_Med_py3cdw);
		VR_off_High_radw = -sqrt(VR_off_High_py1dw * VR_off_High_py1dw + VR_off_High_py2dw * VR_off_High_py2dw + VR_off_High_py3adw * VR_off_High_py3adw + VR_off_High_py3bdw * VR_off_High_py3bdw + VR_off_High_py3cdw * VR_off_High_py3cdw);
		VR_on_Low_radw = -sqrt(VR_on_Low_py1dw * VR_on_Low_py1dw + VR_on_Low_py2dw * VR_on_Low_py2dw + VR_on_Low_py3adw * VR_on_Low_py3adw + VR_on_Low_py3bdw * VR_on_Low_py3bdw + VR_on_Low_py3cdw * VR_on_Low_py3cdw);
		VR_on_Med_radw = -sqrt(VR_on_Med_py1dw * VR_on_Med_py1dw + VR_on_Med_py2dw * VR_on_Med_py2dw + VR_on_Med_py3adw * VR_on_Med_py3adw + VR_on_Med_py3bdw * VR_on_Med_py3bdw + VR_on_Med_py3cdw * VR_on_Med_py3cdw);
		VR_on_High_radw = -sqrt(VR_on_High_py1dw * VR_on_High_py1dw + VR_on_High_py2dw * VR_on_High_py2dw + VR_on_High_py3adw * VR_on_High_py3adw + VR_on_High_py3bdw * VR_on_High_py3bdw + VR_on_High_py3cdw * VR_on_High_py3cdw);
		SR_h_Low_radw = -sqrt(SR_h_Low_py1dw * SR_h_Low_py1dw + SR_h_Low_py2dw * SR_h_Low_py2dw + SR_h_Low_py3adw * SR_h_Low_py3adw + SR_h_Low_py3bdw * SR_h_Low_py3bdw + SR_h_Low_py3cdw * SR_h_Low_py3cdw);
		SR_h_Med_radw = -sqrt(SR_h_Med_py1dw * SR_h_Med_py1dw + SR_h_Med_py2dw * SR_h_Med_py2dw + SR_h_Med_py3adw * SR_h_Med_py3adw + SR_h_Med_py3bdw * SR_h_Med_py3bdw + SR_h_Med_py3cdw * SR_h_Med_py3cdw);
		SR_h_High_radw = -sqrt(SR_h_High_py1dw * SR_h_High_py1dw + SR_h_High_py2dw * SR_h_High_py2dw + SR_h_High_py3adw * SR_h_High_py3adw + SR_h_High_py3bdw * SR_h_High_py3bdw + SR_h_High_py3cdw * SR_h_High_py3cdw);

		VR_off_Low_raup = sqrt(VR_off_Low_py1up * VR_off_Low_py1up + VR_off_Low_py2up * VR_off_Low_py2up + VR_off_Low_py3aup * VR_off_Low_py3aup + VR_off_Low_py3bup * VR_off_Low_py3bup + VR_off_Low_py3cup * VR_off_Low_py3cup);
		VR_off_Med_raup = sqrt(VR_off_Med_py1up * VR_off_Med_py1up + VR_off_Med_py2up * VR_off_Med_py2up + VR_off_Med_py3aup * VR_off_Med_py3aup + VR_off_Med_py3bup * VR_off_Med_py3bup + VR_off_Med_py3cup * VR_off_Med_py3cup);
		VR_off_High_raup = sqrt(VR_off_High_py1up * VR_off_High_py1up + VR_off_High_py2up * VR_off_High_py2up + VR_off_High_py3aup * VR_off_High_py3aup + VR_off_High_py3bup * VR_off_High_py3bup + VR_off_High_py3cup * VR_off_High_py3cup);
		VR_on_Low_raup = sqrt(VR_on_Low_py1up * VR_on_Low_py1up + VR_on_Low_py2up * VR_on_Low_py2up + VR_on_Low_py3aup * VR_on_Low_py3aup + VR_on_Low_py3bup * VR_on_Low_py3bup + VR_on_Low_py3cup * VR_on_Low_py3cup);
		VR_on_Med_raup = sqrt(VR_on_Med_py1up * VR_on_Med_py1up + VR_on_Med_py2up * VR_on_Med_py2up + VR_on_Med_py3aup * VR_on_Med_py3aup + VR_on_Med_py3bup * VR_on_Med_py3bup + VR_on_Med_py3cup * VR_on_Med_py3cup);
		VR_on_High_raup = sqrt(VR_on_High_py1up * VR_on_High_py1up + VR_on_High_py2up * VR_on_High_py2up + VR_on_High_py3aup * VR_on_High_py3aup + VR_on_High_py3bup * VR_on_High_py3bup + VR_on_High_py3cup * VR_on_High_py3cup);
		SR_h_Low_raup = sqrt(SR_h_Low_py1up * SR_h_Low_py1up + SR_h_Low_py2up * SR_h_Low_py2up + SR_h_Low_py3aup * SR_h_Low_py3aup + SR_h_Low_py3bup * SR_h_Low_py3bup + SR_h_Low_py3cup * SR_h_Low_py3cup);
		SR_h_Med_raup = sqrt(SR_h_Med_py1up * SR_h_Med_py1up + SR_h_Med_py2up * SR_h_Med_py2up + SR_h_Med_py3aup * SR_h_Med_py3aup + SR_h_Med_py3bup * SR_h_Med_py3bup + SR_h_Med_py3cup * SR_h_Med_py3cup);
		SR_h_High_raup = sqrt(SR_h_High_py1up * SR_h_High_py1up + SR_h_High_py2up * SR_h_High_py2up + SR_h_High_py3aup * SR_h_High_py3aup + SR_h_High_py3bup * SR_h_High_py3bup + SR_h_High_py3cup * SR_h_High_py3cup);

		//VR_off_Low_radw =	reduceLarge(VR_off_Low_radw);
		//VR_off_Med_radw =	reduceLarge(VR_off_Med_radw);
		//VR_off_High_radw =  reduceLarge(VR_off_High_radw); 
		//VR_on_Low_radw =	reduceLarge(VR_on_Low_radw);
		//VR_on_Med_radw =	reduceLarge(VR_on_Med_radw);
		//VR_on_High_radw =	reduceLarge(VR_on_High_radw);
		//SR_h_Low_radw = 	reduceLarge(SR_h_Low_radw);
		//SR_h_Med_radw = 	reduceLarge(SR_h_Med_radw);
		//SR_h_High_radw =	reduceLarge(SR_h_High_radw);
		//VR_off_Low_raup =	reduceLarge(VR_off_Low_raup);
		//VR_off_Med_raup =   reduceLarge(VR_off_Med_raup);
		//VR_off_High_raup =  reduceLarge(VR_off_High_raup);
		//VR_on_Low_raup =	reduceLarge(VR_on_Low_raup);
		//VR_on_Med_raup =	reduceLarge(VR_on_Med_raup);
		//VR_on_High_raup =	reduceLarge(VR_on_High_raup);
		//SR_h_Low_raup = 	reduceLarge(SR_h_Low_raup);
		//SR_h_Med_raup = 	reduceLarge(SR_h_Med_raup);
		//SR_h_High_raup =	reduceLarge(SR_h_High_raup);
		//if(VR_off_High_radw == -1 || VR_off_High_raup == 1) cout << mcChannelNumber <<endl;

		mCutflow->startCut(Cutflow::NO_HIST);
    }
	delete mCutflow;
}

int MainProcess(int argc, char* argv[])
{
    cout<<"*******************************Start**********************************"<<endl;

    //argv[1]表示datamode，datamode=background表示不需要分析文件名信息
    //datamode=signal表示需分析文件名信息
    //datamode=data表示使用针对data的cut
    //argv[2]表示samplemode，可为16a,16d,16aaf2,16daf2,data,bkg
    //argv[3]表示filepath
    std::string pathDATA = argv[1];

    //因为argv[0]是程序本身,argv[1],[2]是条件判断,argv[3]是path,所以这里要减4,之后要加4
    for (int i =0; i<argc-2; i++)
    {
		std::string filename = argv[i+2];
		if(getfileinfo(pathDATA, filename)){
			file = filename;
			maincut(pathDATA,filename);
		}
    }
    std::cout << "******************E N D****************************************" << std::endl;

    return 0;
}

