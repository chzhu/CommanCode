#define selector_cxx
#include "selector.h"
#include "glog/logging.h"
#include "run2ComFunc.h"
#include <vector>
#include "Cutflow.h"
#include "Utils.h"
#include "PhyUtils.h"
#include "MetaDB.h"
#include <TLorentzVector.h>
#include <algorithm>
#include <string>
#include "json.hpp"
#include "Point3D.h"
#include <TGraph2D.h>

using json = nlohmann::json;

std::string file;

void adjustPoints(vector<Point3D>& Ps){
	bool Needadjust(true);
	while(Needadjust){
		Needadjust = false;
		for(int i = 0; i < Ps.size(); i++){
			//If thee value larger than what we expected!
			if(fabs(Ps[i].Z()) > 0.6){
				auto newP = Ps[i].getMinXYDistancePointNo0(Ps);
				Ps[i] = newP;
				// Need to look again to see if it still need adjustment
				Needadjust = true;
			}
		}
	}
}

void addPointToTG2D(TGraph2D* g, vector<Point3D> Ps){
	adjustPoints(Ps);
	for(int i = 0; i < Ps.size(); i++){
		Point3D P = Ps[i];
		g->SetPoint(i,P.X(),P.Y(),P.Z());
	}
}

void selector::Loop()
{
   if (fChain == 0) return;

   Long64_t nentries = fChain->GetEntries();

   Long64_t nbytes = 0, nb = 0;

	TFile* f = new TFile("syst.root", "recreate");
   //Json redader
   std::ifstream cFile("/publicfs/atlas/atlasnew/SUSY/users/zhucz/framework/theoUncertainty/treeToPlot/dataset.json");

   CHECK(cFile) << "JSON File " << " not Found! Loading terminated";

   json pointData;
   cFile >> pointData;

	//Output plots
   TGraph2D* syst_SR_h_Low_qcdw   = new TGraph2D();
   TGraph2D* syst_SR_h_Low_qcup   = new TGraph2D();
   TGraph2D* syst_SR_h_Low_scdw   = new TGraph2D();
   TGraph2D* syst_SR_h_Low_scup   = new TGraph2D();
   TGraph2D* syst_SR_h_Med_qcdw   = new TGraph2D();
   TGraph2D* syst_SR_h_Med_qcup   = new TGraph2D();
   TGraph2D* syst_SR_h_Med_scdw   = new TGraph2D();
   TGraph2D* syst_SR_h_Med_scup   = new TGraph2D();
   TGraph2D* syst_SR_h_High_qcdw  = new TGraph2D();
   TGraph2D* syst_SR_h_High_qcup  = new TGraph2D();
   TGraph2D* syst_SR_h_High_scdw  = new TGraph2D();
   TGraph2D* syst_SR_h_High_scup  = new TGraph2D();
   TGraph2D* syst_VR_off_Low_qcdw = new TGraph2D();
   TGraph2D* syst_VR_off_Low_qcup = new TGraph2D();
   TGraph2D* syst_VR_off_Low_scdw = new TGraph2D();
   TGraph2D* syst_VR_off_Low_scup = new TGraph2D();
   TGraph2D* syst_VR_off_Med_qcdw = new TGraph2D();
   TGraph2D* syst_VR_off_Med_qcup = new TGraph2D();
   TGraph2D* syst_VR_off_Med_scdw = new TGraph2D();
   TGraph2D* syst_VR_off_Med_scup = new TGraph2D();
   TGraph2D* syst_VR_off_High_qcdw= new TGraph2D();
   TGraph2D* syst_VR_off_High_qcup= new TGraph2D();
   TGraph2D* syst_VR_off_High_scdw= new TGraph2D();
   TGraph2D* syst_VR_off_High_scup= new TGraph2D();
   TGraph2D* syst_VR_on_Low_qcdw  = new TGraph2D();
   TGraph2D* syst_VR_on_Low_qcup  = new TGraph2D();
   TGraph2D* syst_VR_on_Low_scdw  = new TGraph2D();
   TGraph2D* syst_VR_on_Low_scup  = new TGraph2D();
   TGraph2D* syst_VR_on_Med_qcdw  = new TGraph2D();
   TGraph2D* syst_VR_on_Med_qcup  = new TGraph2D();
   TGraph2D* syst_VR_on_Med_scdw  = new TGraph2D();
   TGraph2D* syst_VR_on_Med_scup  = new TGraph2D();
   TGraph2D* syst_VR_on_High_qcdw = new TGraph2D();
   TGraph2D* syst_VR_on_High_qcup = new TGraph2D();
   TGraph2D* syst_VR_on_High_scdw = new TGraph2D();
   TGraph2D* syst_VR_on_High_scup = new TGraph2D();
   TGraph2D* syst_VR_off_Low_radw = new TGraph2D();
   TGraph2D* syst_VR_off_Med_radw = new TGraph2D();
   TGraph2D* syst_VR_off_High_radw= new TGraph2D();
   TGraph2D* syst_SR_h_Low_radw   = new TGraph2D();
   TGraph2D* syst_SR_h_Med_radw   = new TGraph2D();
   TGraph2D* syst_SR_h_High_radw  = new TGraph2D();
   TGraph2D* syst_VR_on_Low_radw  = new TGraph2D();
   TGraph2D* syst_VR_on_Med_radw  = new TGraph2D();
   TGraph2D* syst_VR_on_High_radw = new TGraph2D();
   TGraph2D* syst_VR_off_Low_raup = new TGraph2D();
   TGraph2D* syst_VR_off_Med_raup = new TGraph2D();
   TGraph2D* syst_VR_off_High_raup= new TGraph2D();
   TGraph2D* syst_SR_h_Low_raup   = new TGraph2D();
   TGraph2D* syst_SR_h_Med_raup   = new TGraph2D();
   TGraph2D* syst_SR_h_High_raup  = new TGraph2D();
   TGraph2D* syst_VR_on_Low_raup  = new TGraph2D();
   TGraph2D* syst_VR_on_Med_raup  = new TGraph2D();
   TGraph2D* syst_VR_on_High_raup = new TGraph2D();

	vector<Point3D> Points_SR_h_Low_qcdw;
	vector<Point3D> Points_SR_h_Low_qcup;
	vector<Point3D> Points_SR_h_Low_scdw;
	vector<Point3D> Points_SR_h_Low_scup;
	vector<Point3D> Points_SR_h_Med_qcdw;
	vector<Point3D> Points_SR_h_Med_qcup;
	vector<Point3D> Points_SR_h_Med_scdw;
	vector<Point3D> Points_SR_h_Med_scup;
	vector<Point3D> Points_SR_h_High_qcdw;
	vector<Point3D> Points_SR_h_High_qcup;
	vector<Point3D> Points_SR_h_High_scdw;
	vector<Point3D> Points_SR_h_High_scup;
	vector<Point3D> Points_VR_off_Low_qcdw;
	vector<Point3D> Points_VR_off_Low_qcup;
	vector<Point3D> Points_VR_off_Low_scdw;
	vector<Point3D> Points_VR_off_Low_scup;
	vector<Point3D> Points_VR_off_Med_qcdw;
	vector<Point3D> Points_VR_off_Med_qcup;
	vector<Point3D> Points_VR_off_Med_scdw;
	vector<Point3D> Points_VR_off_Med_scup;
	vector<Point3D> Points_VR_off_High_qcdw;
	vector<Point3D> Points_VR_off_High_qcup;
	vector<Point3D> Points_VR_off_High_scdw;
	vector<Point3D> Points_VR_off_High_scup;
	vector<Point3D> Points_VR_on_Low_qcdw;
	vector<Point3D> Points_VR_on_Low_qcup;
	vector<Point3D> Points_VR_on_Low_scdw;
	vector<Point3D> Points_VR_on_Low_scup;
	vector<Point3D> Points_VR_on_Med_qcdw;
	vector<Point3D> Points_VR_on_Med_qcup;
	vector<Point3D> Points_VR_on_Med_scdw;
	vector<Point3D> Points_VR_on_Med_scup;
	vector<Point3D> Points_VR_on_High_qcdw;
	vector<Point3D> Points_VR_on_High_qcup;
	vector<Point3D> Points_VR_on_High_scdw;
	vector<Point3D> Points_VR_on_High_scup;
	vector<Point3D> Points_VR_off_Low_radw;
	vector<Point3D> Points_VR_off_Med_radw;
	vector<Point3D> Points_VR_off_High_radw;
	vector<Point3D> Points_SR_h_Low_radw;
	vector<Point3D> Points_SR_h_Med_radw;
	vector<Point3D> Points_SR_h_High_radw;
	vector<Point3D> Points_VR_on_Low_radw;
	vector<Point3D> Points_VR_on_Med_radw;
	vector<Point3D> Points_VR_on_High_radw;
	vector<Point3D> Points_VR_off_Low_raup;
	vector<Point3D> Points_VR_off_Med_raup;
	vector<Point3D> Points_VR_off_High_raup;
	vector<Point3D> Points_SR_h_Low_raup;
	vector<Point3D> Points_SR_h_Med_raup;
	vector<Point3D> Points_SR_h_High_raup;
	vector<Point3D> Points_VR_on_Low_raup;
	vector<Point3D> Points_VR_on_Med_raup;
	vector<Point3D> Points_VR_on_High_raup;

   for (Long64_t jentry=0; jentry<nentries;jentry++) {
      Long64_t ientry = LoadTree(jentry);
      if (ientry < 0) break;
      nb = fChain->GetEntry(jentry);   nbytes += nb;
      if (0 == jentry%100000)
      	cout << jentry << " entry of " << nentries << "entries" << endl;

	  string MCID = to_string(mcChannelNumber);

	  double X = pointData[MCID][0];
	  double Y = pointData[MCID][1];

	  Points_SR_h_Low_qcdw.emplace_back(Point3D(X,Y,SR_h_Low_qcdw));
	  Points_SR_h_Low_qcup.emplace_back(Point3D(X,Y,SR_h_Low_qcup));
	  Points_SR_h_Low_scdw.emplace_back(Point3D(X,Y,SR_h_Low_scdw));
	  Points_SR_h_Low_scup.emplace_back(Point3D(X,Y,SR_h_Low_scup));
	  Points_SR_h_Med_qcdw.emplace_back(Point3D(X,Y,SR_h_Med_qcdw));
	  Points_SR_h_Med_qcup.emplace_back(Point3D(X,Y,SR_h_Med_qcup));
	  Points_SR_h_Med_scdw.emplace_back(Point3D(X,Y,SR_h_Med_scdw));
	  Points_SR_h_Med_scup.emplace_back(Point3D(X,Y,SR_h_Med_scup));
	  Points_SR_h_High_qcdw.emplace_back(Point3D(X,Y,SR_h_High_qcdw));
	  Points_SR_h_High_qcup.emplace_back(Point3D(X,Y,SR_h_High_qcup));
	  Points_SR_h_High_scdw.emplace_back(Point3D(X,Y,SR_h_High_scdw));
	  Points_SR_h_High_scup.emplace_back(Point3D(X,Y,SR_h_High_scup));
	  Points_VR_off_Low_qcdw.emplace_back(Point3D(X,Y,VR_off_Low_qcdw));
	  Points_VR_off_Low_qcup.emplace_back(Point3D(X,Y,VR_off_Low_qcup));
	  Points_VR_off_Low_scdw.emplace_back(Point3D(X,Y,VR_off_Low_scdw));
	  Points_VR_off_Low_scup.emplace_back(Point3D(X,Y,VR_off_Low_scup));
	  Points_VR_off_Med_qcdw.emplace_back(Point3D(X,Y,VR_off_Med_qcdw));
	  Points_VR_off_Med_qcup.emplace_back(Point3D(X,Y,VR_off_Med_qcup));
	  Points_VR_off_Med_scdw.emplace_back(Point3D(X,Y,VR_off_Med_scdw));
	  Points_VR_off_Med_scup.emplace_back(Point3D(X,Y,VR_off_Med_scup));
	  Points_VR_off_High_qcdw.emplace_back(Point3D(X,Y,VR_off_High_qcdw));
	  Points_VR_off_High_qcup.emplace_back(Point3D(X,Y,VR_off_High_qcup));
	  Points_VR_off_High_scdw.emplace_back(Point3D(X,Y,VR_off_High_scdw));
	  Points_VR_off_High_scup.emplace_back(Point3D(X,Y,VR_off_High_scup));
	  Points_VR_on_Low_qcdw.emplace_back(Point3D(X,Y,VR_on_Low_qcdw));
	  Points_VR_on_Low_qcup.emplace_back(Point3D(X,Y,VR_on_Low_qcup));
	  Points_VR_on_Low_scdw.emplace_back(Point3D(X,Y,VR_on_Low_scdw));
	  Points_VR_on_Low_scup.emplace_back(Point3D(X,Y,VR_on_Low_scup));
	  Points_VR_on_Med_qcdw.emplace_back(Point3D(X,Y,VR_on_Med_qcdw));
	  Points_VR_on_Med_qcup.emplace_back(Point3D(X,Y,VR_on_Med_qcup));
	  Points_VR_on_Med_scdw.emplace_back(Point3D(X,Y,VR_on_Med_scdw));
	  Points_VR_on_Med_scup.emplace_back(Point3D(X,Y,VR_on_Med_scup));
	  Points_VR_on_High_qcdw.emplace_back(Point3D(X,Y,VR_on_High_qcdw));
	  Points_VR_on_High_qcup.emplace_back(Point3D(X,Y,VR_on_High_qcup));
	  Points_VR_on_High_scdw.emplace_back(Point3D(X,Y,VR_on_High_scdw));
	  Points_VR_on_High_scup.emplace_back(Point3D(X,Y,VR_on_High_scup));
	  Points_VR_off_Low_radw.emplace_back(Point3D(X,Y,VR_off_Low_radw));
	  Points_VR_off_Med_radw.emplace_back(Point3D(X,Y,VR_off_Med_radw));
	  Points_VR_off_High_radw.emplace_back(Point3D(X,Y,VR_off_High_radw));
	  Points_SR_h_Low_radw.emplace_back(Point3D(X,Y,SR_h_Low_radw));
	  Points_SR_h_Med_radw.emplace_back(Point3D(X,Y,SR_h_Med_radw));
	  Points_SR_h_High_radw.emplace_back(Point3D(X,Y,SR_h_High_radw));
	  Points_VR_on_Low_radw.emplace_back(Point3D(X,Y,VR_on_Low_radw));
	  Points_VR_on_Med_radw.emplace_back(Point3D(X,Y,VR_on_Med_radw));
	  Points_VR_on_High_radw.emplace_back(Point3D(X,Y,VR_on_High_radw));
	  Points_VR_off_Low_raup.emplace_back(Point3D(X,Y,VR_off_Low_raup));
	  Points_VR_off_Med_raup.emplace_back(Point3D(X,Y,VR_off_Med_raup));
	  Points_VR_off_High_raup.emplace_back(Point3D(X,Y,VR_off_High_raup));
	  Points_SR_h_Low_raup.emplace_back(Point3D(X,Y,SR_h_Low_raup));
	  Points_SR_h_Med_raup.emplace_back(Point3D(X,Y,SR_h_Med_raup));
	  Points_SR_h_High_raup.emplace_back(Point3D(X,Y,SR_h_High_raup));
	  Points_VR_on_Low_raup.emplace_back(Point3D(X,Y,VR_on_Low_raup));
	  Points_VR_on_Med_raup.emplace_back(Point3D(X,Y,VR_on_Med_raup));
	  Points_VR_on_High_raup.emplace_back(Point3D(X,Y,VR_on_High_raup));

    }

	  addPointToTG2D(syst_SR_h_Low_qcdw, Points_SR_h_Low_qcdw);
	  addPointToTG2D(syst_SR_h_Low_qcup, Points_SR_h_Low_qcup);
	  addPointToTG2D(syst_SR_h_Low_scdw, Points_SR_h_Low_scdw);
	  addPointToTG2D(syst_SR_h_Low_scup, Points_SR_h_Low_scup);
	  addPointToTG2D(syst_SR_h_Med_qcdw, Points_SR_h_Med_qcdw);
	  addPointToTG2D(syst_SR_h_Med_qcup, Points_SR_h_Med_qcup);
	  addPointToTG2D(syst_SR_h_Med_scdw, Points_SR_h_Med_scdw);
	  addPointToTG2D(syst_SR_h_Med_scup, Points_SR_h_Med_scup);
	  addPointToTG2D(syst_SR_h_High_qcdw, Points_SR_h_High_qcdw);
	  addPointToTG2D(syst_SR_h_High_qcup, Points_SR_h_High_qcup);
	  addPointToTG2D(syst_SR_h_High_scdw, Points_SR_h_High_scdw);
	  addPointToTG2D(syst_SR_h_High_scup, Points_SR_h_High_scup);
	  addPointToTG2D(syst_VR_off_Low_qcdw, Points_VR_off_Low_qcdw);
	  addPointToTG2D(syst_VR_off_Low_qcup, Points_VR_off_Low_qcup);
	  addPointToTG2D(syst_VR_off_Low_scdw, Points_VR_off_Low_scdw);
	  addPointToTG2D(syst_VR_off_Low_scup, Points_VR_off_Low_scup);
	  addPointToTG2D(syst_VR_off_Med_qcdw, Points_VR_off_Med_qcdw);
	  addPointToTG2D(syst_VR_off_Med_qcup, Points_VR_off_Med_qcup);
	  addPointToTG2D(syst_VR_off_Med_scdw, Points_VR_off_Med_scdw);
	  addPointToTG2D(syst_VR_off_Med_scup, Points_VR_off_Med_scup);
	  addPointToTG2D(syst_VR_off_High_qcdw, Points_VR_off_High_qcdw);
	  addPointToTG2D(syst_VR_off_High_qcup, Points_VR_off_High_qcup);
	  addPointToTG2D(syst_VR_off_High_scdw, Points_VR_off_High_scdw);
	  addPointToTG2D(syst_VR_off_High_scup, Points_VR_off_High_scup);
	  addPointToTG2D(syst_VR_on_Low_qcdw, Points_VR_on_Low_qcdw);
	  addPointToTG2D(syst_VR_on_Low_qcup, Points_VR_on_Low_qcup);
	  addPointToTG2D(syst_VR_on_Low_scdw, Points_VR_on_Low_scdw);
	  addPointToTG2D(syst_VR_on_Low_scup, Points_VR_on_Low_scup);
	  addPointToTG2D(syst_VR_on_Med_qcdw, Points_VR_on_Med_qcdw);
	  addPointToTG2D(syst_VR_on_Med_qcup, Points_VR_on_Med_qcup);
	  addPointToTG2D(syst_VR_on_Med_scdw, Points_VR_on_Med_scdw);
	  addPointToTG2D(syst_VR_on_Med_scup, Points_VR_on_Med_scup);
	  addPointToTG2D(syst_VR_on_High_qcdw, Points_VR_on_High_qcdw);
	  addPointToTG2D(syst_VR_on_High_qcup, Points_VR_on_High_qcup);
	  addPointToTG2D(syst_VR_on_High_scdw, Points_VR_on_High_scdw);
	  addPointToTG2D(syst_VR_on_High_scup, Points_VR_on_High_scup);
	  addPointToTG2D(syst_VR_off_Low_radw, Points_VR_off_Low_radw);
	  addPointToTG2D(syst_VR_off_Med_radw, Points_VR_off_Med_radw);
	  addPointToTG2D(syst_VR_off_High_radw, Points_VR_off_High_radw);
	  addPointToTG2D(syst_SR_h_Low_radw, Points_SR_h_Low_radw);
	  addPointToTG2D(syst_SR_h_Med_radw, Points_SR_h_Med_radw);
	  addPointToTG2D(syst_SR_h_High_radw, Points_SR_h_High_radw);
	  addPointToTG2D(syst_VR_on_Low_radw, Points_VR_on_Low_radw);
	  addPointToTG2D(syst_VR_on_Med_radw, Points_VR_on_Med_radw);
	  addPointToTG2D(syst_VR_on_High_radw, Points_VR_on_High_radw);
	  addPointToTG2D(syst_VR_off_Low_raup, Points_VR_off_Low_raup);
	  addPointToTG2D(syst_VR_off_Med_raup, Points_VR_off_Med_raup);
	  addPointToTG2D(syst_VR_off_High_raup, Points_VR_off_High_raup);
	  addPointToTG2D(syst_SR_h_Low_raup, Points_SR_h_Low_raup);
	  addPointToTG2D(syst_SR_h_Med_raup, Points_SR_h_Med_raup);
	  addPointToTG2D(syst_SR_h_High_raup, Points_SR_h_High_raup);
	  addPointToTG2D(syst_VR_on_Low_raup, Points_VR_on_Low_raup);
	  addPointToTG2D(syst_VR_on_Med_raup, Points_VR_on_Med_raup);
	  addPointToTG2D(syst_VR_on_High_raup, Points_VR_on_High_raup);


	  syst_SR_h_Low_qcdw->Write("syst_SR_h_Low_qcdw");
	  syst_SR_h_Low_qcup->Write("syst_SR_h_Low_qcup");
	  syst_SR_h_Low_scdw->Write("syst_SR_h_Low_scdw");
	  syst_SR_h_Low_scup->Write("syst_SR_h_Low_scup");
	  syst_SR_h_Med_qcdw->Write("syst_SR_h_Med_qcdw");
	  syst_SR_h_Med_qcup->Write("syst_SR_h_Med_qcup");
	  syst_SR_h_Med_scdw->Write("syst_SR_h_Med_scdw");
	  syst_SR_h_Med_scup->Write("syst_SR_h_Med_scup");
	  syst_SR_h_High_qcdw->Write("syst_SR_h_High_qcdw");
	  syst_SR_h_High_qcup->Write("syst_SR_h_High_qcup");
	  syst_SR_h_High_scdw->Write("syst_SR_h_High_scdw");
	  syst_SR_h_High_scup->Write("syst_SR_h_High_scup");
	  syst_VR_off_Low_qcdw->Write("syst_VR_off_Low_qcdw");
	  syst_VR_off_Low_qcup->Write("syst_VR_off_Low_qcup");
	  syst_VR_off_Low_scdw->Write("syst_VR_off_Low_scdw");
	  syst_VR_off_Low_scup->Write("syst_VR_off_Low_scup");
	  syst_VR_off_Med_qcdw->Write("syst_VR_off_Med_qcdw");
	  syst_VR_off_Med_qcup->Write("syst_VR_off_Med_qcup");
	  syst_VR_off_Med_scdw->Write("syst_VR_off_Med_scdw");
	  syst_VR_off_Med_scup->Write("syst_VR_off_Med_scup");
	  syst_VR_off_High_qcdw->Write("syst_VR_off_High_qcdw");
	  syst_VR_off_High_qcup->Write("syst_VR_off_High_qcup");
	  syst_VR_off_High_scdw->Write("syst_VR_off_High_scdw");
	  syst_VR_off_High_scup->Write("syst_VR_off_High_scup");
	  syst_VR_on_Low_qcdw->Write("syst_VR_on_Low_qcdw");
	  syst_VR_on_Low_qcup->Write("syst_VR_on_Low_qcup");
	  syst_VR_on_Low_scdw->Write("syst_VR_on_Low_scdw");
	  syst_VR_on_Low_scup->Write("syst_VR_on_Low_scup");
	  syst_VR_on_Med_qcdw->Write("syst_VR_on_Med_qcdw");
	  syst_VR_on_Med_qcup->Write("syst_VR_on_Med_qcup");
	  syst_VR_on_Med_scdw->Write("syst_VR_on_Med_scdw");
	  syst_VR_on_Med_scup->Write("syst_VR_on_Med_scup");
	  syst_VR_on_High_qcdw->Write("syst_VR_on_High_qcdw");
	  syst_VR_on_High_qcup->Write("syst_VR_on_High_qcup");
	  syst_VR_on_High_scdw->Write("syst_VR_on_High_scdw");
	  syst_VR_on_High_scup->Write("syst_VR_on_High_scup");
	  syst_VR_off_Low_radw->Write("syst_VR_off_Low_radw");
	  syst_VR_off_Med_radw->Write("syst_VR_off_Med_radw");
	  syst_VR_off_High_radw->Write("syst_VR_off_High_radw");
	  syst_SR_h_Low_radw->Write("syst_SR_h_Low_radw");
	  syst_SR_h_Med_radw->Write("syst_SR_h_Med_radw");
	  syst_SR_h_High_radw->Write("syst_SR_h_High_radw");
	  syst_VR_on_Low_radw->Write("syst_VR_on_Low_radw");
	  syst_VR_on_Med_radw->Write("syst_VR_on_Med_radw");
	  syst_VR_on_High_radw->Write("syst_VR_on_High_radw");
	  syst_VR_off_Low_raup->Write("syst_VR_off_Low_raup");
	  syst_VR_off_Med_raup->Write("syst_VR_off_Med_raup");
	  syst_VR_off_High_raup->Write("syst_VR_off_High_raup");
	  syst_SR_h_Low_raup->Write("syst_SR_h_Low_raup");
	  syst_SR_h_Med_raup->Write("syst_SR_h_Med_raup");
	  syst_SR_h_High_raup->Write("syst_SR_h_High_raup");
	  syst_VR_on_Low_raup->Write("syst_VR_on_Low_raup");
	  syst_VR_on_Med_raup->Write("syst_VR_on_Med_raup");
	  syst_VR_on_High_raup->Write("syst_VR_on_High_raup");


   f->Write();
   f->Close();
}

int MainProcess(int argc, char* argv[])
{
    cout<<"*******************************Start**********************************"<<endl;

    //argv[1]表示datamode，datamode=background表示不需要分析文件名信息
    //datamode=signal表示需分析文件名信息
    //datamode=data表示使用针对data的cut
    //argv[2]表示samplemode，可为16a,16d,16aaf2,16daf2,data,bkg
    //argv[3]表示filepath
    std::string pathDATA = argv[1];

    //因为argv[0]是程序本身,argv[1],[2]是条件判断,argv[3]是path,所以这里要减4,之后要加4
    for (int i =0; i<argc-2; i++)
    {
		std::string filename = argv[i+2];
		if(getfileinfo(pathDATA, filename)){
			file = filename;
			maincut(pathDATA,filename);
		}
    }
    std::cout << "******************E N D****************************************" << std::endl;

    return 0;
}

