#pragma once
#include <iostream>
#include <fstream>
#include <regex>
#include <string>
#include <vector>
#include "selector.h"
#include "Utils.h"
#include "glog/logging.h"
#include <TChain.h>

std::vector<std::string> getinfo(regex pattern, const string& ip)
{
	std::smatch result;
	std::vector<std::string> ret;
	bool valid = regex_match(ip,result,pattern); 
	DLOG(INFO)<<ip<<"  "<<(valid?"valid":"invalid")<<endl;
	if(valid)
	{       
			for(unsigned int i = 0; i < result.size(); i++){
				ret.push_back(result[i]);
			}
	}
	return ret;
}

//正则匹配，过滤出root文件
bool getfileinfo(string pathDATA, const string& ip)
{
	regex pattern;
	pattern = "(.*\\.root)";
	std::vector<std::string> result = getinfo(pattern,ip);
	return (result.size() != 0);
}

void maincut(string path, string filename, bool doQuick = false)
{
	std::string rootfile = path + filename;
	std::vector<std::string> chains;
	chains = Utils::getTreeList(rootfile,".*syst.*");
	for(auto chainname : chains){
		LOG(INFO) << "**********Start cutting " << filename << "*********";
		LOG(INFO)<<"start tree name: "<<chainname.c_str();
		TChain *chain = new TChain(chainname.c_str());
		chain->Add(rootfile.c_str());
		selector* analysis = new selector(chain);

		analysis->Loop();
		LOG(INFO) << "**********End of cutting " << filename << "********";

		delete analysis;
		delete chain;
	}

}
