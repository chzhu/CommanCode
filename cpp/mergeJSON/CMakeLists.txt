# CMakeLists.txt for event package. It creates a library with dictionary and a main program
cmake_minimum_required(VERSION 2.8 FATAL_ERROR)
project(Selector)

# You need to tell CMake where to find the ROOT installation. This can be done in a number of ways:
#   - ROOT built with classic configure/make use the provided $ROOTSYS/etc/cmake/FindROOT.cmake
#   - ROOT built with CMake. Add in CMAKE_PREFIX_PATH the installation prefix for ROOT
list(APPEND CMAKE_PREFIX_PATH $ENV{ROOTSYS})

#---Locate the ROOT package and defines a number of variables (e.g. ROOT_INCLUDE_DIRS)
find_package(ROOT)

#Check if Root included
if(!ROOT_FOUND)
	message(FATAL_ERROR "Fetal: Root pacakge not found!")
endif()

#Shared libary files
aux_source_directory(. DIR_LIB_SRCS)
aux_source_directory(/afs/ihep.ac.cn/users/z/zhucz/codes/cpp/MiniAnalysis/src/ DIR_LIB_BASE_SRCS)
include_directories(./ /afs/ihep.ac.cn/users/z/zhucz/codes/cpp/MiniAnalysis/include/)

#---Define useful ROOT functions and macros (e.g. ROOT_GENERATE_DICTIONARY)
#We used TROOT.h, so we need ROOT_USE_FILE
include(${ROOT_USE_FILE})


#---Create  a main program using the library
add_executable(mergeJson.x ${DIR_LIB_SRCS} ${DIR_LIB_BASE_SRCS})
target_link_libraries(mergeJson.x ${ROOT_LIBRARIES})
