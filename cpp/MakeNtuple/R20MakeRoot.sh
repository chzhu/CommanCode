source Compile.sh
wait
#-------------------------run/R20--------------------------
filepath=/publicfs/atlas/atlasnew/SUSY/users/liuy/Tau_v28/

#------------------------nocut------------------------
nohup ./tau_analysis -r R20 -t TauNoCut -m signal -o run/R20/nocut/Ztt.root -i "$filepath"hist-361108.root > run/R20/nocut/Ztt.log &
nohup ./tau_analysis -r R20 -t TauNoCut -m signal -o run/R20/nocut/Top.root -i "$filepath"hist-410000.root > run/R20/nocut/Top.log &
nohup ./tau_analysis -r R20 -t TauNoCut -o run/R20/nocut/W.root -i "$filepath"hist-361100.root,"$filepath"hist-361101.root,"$filepath"hist-361102.root,"$filepath"hist-361103.root,"$filepath"hist-361104.root,"$filepath"hist-361105.root > run/R20/nocut/W.log &
nohup ./tau_analysis -r R20 -t TauNoCut -o run/R20/nocut/Zll.root -i "$filepath"hist-361106.root,"$filepath"hist-361107.root > run/R20/nocut/Zll.log &
nohup ./tau_analysis -r R20 -t TauNoCut -m data -o run/R20/nocut/data.root -i "$filepath"data.root > run/R20/nocut/data.log &

#------------------------loose------------------------
nohup ./tau_analysis -r R20 -t TauLoose -m signal -o run/R20/loose/Ztt.root -i "$filepath"hist-361108.root > run/R20/loose/Ztt.log &
nohup ./tau_analysis -r R20 -t TauLoose -m signal -o run/R20/loose/Top.root -i "$filepath"hist-410000.root > run/R20/loose/Top.log &
nohup ./tau_analysis -r R20 -t TauLoose -o run/R20/loose/W.root -i "$filepath"hist-361100.root,"$filepath"hist-361101.root,"$filepath"hist-361102.root,"$filepath"hist-361103.root,"$filepath"hist-361104.root,"$filepath"hist-361105.root > run/R20/loose/W.log &
nohup ./tau_analysis -r R20 -t TauLoose -o run/R20/loose/Zll.root -i "$filepath"hist-361106.root,"$filepath"hist-361107.root > run/R20/loose/Zll.log &
nohup ./tau_analysis -r R20 -t TauLoose -m data -o run/R20/loose/data.root -i "$filepath"data.root > run/R20/loose/data.log &

#------------------------medium------------------------
nohup ./tau_analysis -r R20 -m signal -o run/R20/medium/Ztt.root -i "$filepath"hist-361108.root > run/R20/medium/Ztt.log &
nohup ./tau_analysis -r R20 -m signal -o run/R20/medium/Top.root -i "$filepath"hist-410000.root > run/R20/medium/Top.log &
nohup ./tau_analysis -r R20 -o run/R20/medium/W.root -i "$filepath"hist-361100.root,"$filepath"hist-361101.root,"$filepath"hist-361102.root,"$filepath"hist-361103.root,"$filepath"hist-361104.root,"$filepath"hist-361105.root > run/R20/medium/W.log &
nohup ./tau_analysis -r R20 -o run/R20/medium/Zll.root -i "$filepath"hist-361106.root,"$filepath"hist-361107.root > run/R20/medium/Zll.log &
nohup ./tau_analysis -r R20 -m data -o run/R20/medium/data.root -i "$filepath"data.root > run/R20/medium/data.log &

#------------------------tight------------------------
nohup ./tau_analysis -r R20 -t TauTight -m signal -o run/R20/tight/Ztt.root -i "$filepath"hist-361108.root > run/R20/tight/Ztt.log &
nohup ./tau_analysis -r R20 -t TauTight -m signal -o run/R20/tight/Top.root -i "$filepath"hist-410000.root > run/R20/tight/Top.log &
nohup ./tau_analysis -r R20 -t TauTight -o run/R20/tight/W.root -i "$filepath"hist-361100.root,"$filepath"hist-361101.root,"$filepath"hist-361102.root,"$filepath"hist-361103.root,"$filepath"hist-361104.root,"$filepath"hist-361105.root > run/R20/tight/W.log &
nohup ./tau_analysis -r R20 -t TauTight -o run/R20/tight/Zll.root -i "$filepath"hist-361106.root,"$filepath"hist-361107.root > run/R20/tight/Zll.log &
nohup ./tau_analysis -r R20 -t TauTight -m data -o run/R20/tight/data.root -i "$filepath"data.root > run/R20/tight/data.log &

