#include <map>
#include <TSystem.h>
#include <functional>
#include <TFile.h>
#include <TH1.h>
#include <iostream>
#include "Region.h"

#ifndef tau_h
#define tau_h

class tau{

	public:

		double weight = 1;
		double exceptionWei = 1;
		int nTrack = 0;
		bool useWeight = true, isOS = true, isTrueTau = true, isLepFake = true, one_pron = true, three_pron = true;
		int TruDecayMode = -1;
		int RecoDecayMode = -1;
		std::function<bool()> tauIDcut;
		bool TauSelection = true, MuonSelection = true, BaseSelection = true, SR = true, WCR = true;
		bool MCR = true, TopCR = true;
		bool TauIDL = true, TauIDM = true, TauIDT = true;

		//variables we are using
		double tauTruth_pt = -1, tauReco_pt = -1, tauAlltracked_pt = -1, dEta = -1;
		double TauReco_eta = -1, VisMass = -1, MuReco_pt = -1, MuReco_eta = -1, MET = -1, mu = -1;

		//Now we are using in our study
		std::function<double()> deltaEta = [&] {return dEta; };
		std::function<double()> upsilon = [&] {return ((2 * tauAlltracked_pt - tauReco_pt) / tauReco_pt); };
		std::function<double()> NTrack = [&] {return (double)nTrack; };
		std::function<double()> Mu = [&]{ return (double)mu; };
		std::function<double()> TauPt = [&] {return (double)tauReco_pt; };
		std::function<double()> Met = [&] {return MET; };
		std::function<double()> MuEta = [&] {return MuReco_eta; };
		std::function<double()> TauEta = [&] {return TauReco_eta; };
		std::function<double()> MuPt = [&] {return MuReco_pt; };
		std::function<double()> LepHadVisMass = [&] {return VisMass; };

		std::vector<Region*> regions;
		std::string outFile;

		tau(std::string outfilename = "output.root"){
			outFile = outfilename;
			SetTauWP("TauMedium");
		}

		void SetFileName(std::string name) {
			outFile = name;
		}

		void setFillWeight(double exceptionWei){
			useWeight = false;
			this->exceptionWei = exceptionWei;
		}
		void SetTauWP(std::string WP){
			if(WP == "TauNoCut") 	tauIDcut = [&] {return true;};
			else if(WP == "TauLoose") tauIDcut = [&] {return TauIDL;};
			else if(WP == "TauMedium") tauIDcut = [&] {return TauIDM;};
			else if(WP == "TauTight") tauIDcut = [&] {return TauIDT;};
			else{
					std::cout<<"This may not happen, you may change the code in main"<<std::endl
							<<"BTW, choose to defalt WP: TauMedium in case"<<std::endl;;
					tauIDcut = [&] {return TauIDM;};
			}
		}

		//register default regions, details in tau.cxx
		void registerRegions();
		//Do nothing in initial class. Usage is extending extra region in child class
		virtual void registerExtraRegions(){};

		Region* addRegion(std::function<bool()> cut) {
			Region* region = new Region(cut);
			regions.emplace_back(region);
			return region;
		};

		void FillRegions() {
			for (auto region : regions) {
					if (useWeight)	region->fillHists(weight);
					else region->fillHists(exceptionWei);
			}
		}

		//Root is not so smart to autofill hists included in map, so we should fill it manually
		void WriteHists() {
			TFile* f = new TFile(outFile.c_str(), "RECREATE");
			for (auto region : regions)
			{
				auto fillHists = region->hists;
				std::map<TH1*, std::function<double()>>::iterator iter;
				for (iter = fillHists.begin(); iter != fillHists.end(); iter++) {
					iter->first->Write();
				}
			}
			f->Close();
		}
};

//Untils
/*template <class T, class ...Args>
bool CalAnd(T head, Args... rest)
{    
    return (head && CalAnd(rest...));
}

template <class... T, class ...Args>
bool CalOr(T head, Args... rest)
{
	return (head || CalOr(rest...));
}
*/
#endif
