#include "tau.h"

#ifndef tauSubClass_h
#define tauSubClass_h

class dataTau : public tau{
public:
	dataTau(std::string outfilename = "output.root") :tau(outfilename) {};
	void registerExtraRegions() {
		//loop for different Reco decaymode
		for (int i = 0; i < 5; ++i) {
			std::string TruDecayTag, RecoDecayTag;
			int Nbins(0), NbinsNT(12),NbinsDE(10);
			double LowValue(0), UpValue(0);

			double LowValueNT(0),UpValueNT(12);
			double LowValueDE(0), UpValueDE(2.5);
			if (0 == i) {
				RecoDecayTag = "_Reco1";
				Nbins = 15;
				LowValue = 0;
				UpValue = 1.5;
				UpValueNT = 5;
				NbinsNT = 5;
			}else if (1 == i) {
				RecoDecayTag = "_Reco2";
				Nbins = 15;
				LowValue = -1;
				UpValue = 1.5;
			}else if (2 == i) {
				RecoDecayTag = "_Reco3";
				Nbins = 15;
				LowValue = -1;
				UpValue = 0.5;
			}else if (3 == i) {
				RecoDecayTag = "_Reco4";
				Nbins = 10;
				LowValue = 0.5;
				UpValue = 1.5;
			}else if (4 == i) {
				RecoDecayTag = "_Reco5";
				Nbins = 15;
				LowValue = 0;
				UpValue = 1.5;
			}

			HistTemplate upsilonHT(upsilon, Nbins, LowValue, UpValue);
			HistTemplate NTrackHT(NTrack, NbinsNT, LowValueNT, UpValueNT);
			HistTemplate deltaEtaHT(deltaEta, NbinsDE, LowValueDE, UpValueDE);

			Region* regionWCROS_1 = addRegion([&,i] {return (WCR && tauIDcut() && one_pron && isOS && RecoDecayMode == i);});
			regionWCROS_1->AddHist(upsilonHT, "Upsilon_WCR_OS_1" + RecoDecayTag);
			regionWCROS_1->AddHist(NTrackHT, "NTrack_WCR_OS_1" + RecoDecayTag);
			regionWCROS_1->AddHist(deltaEtaHT, "DeltaEta_WCR_OS_1" + RecoDecayTag);

			Region* regionWCROS_3 = addRegion([&,i] {return (WCR && tauIDcut() && three_pron && isOS && RecoDecayMode == i);});
			regionWCROS_3->AddHist(upsilonHT, "Upsilon_WCR_OS_3" + RecoDecayTag);
			regionWCROS_3->AddHist(NTrackHT, "NTrack_WCR_OS_3" + RecoDecayTag);
			regionWCROS_3->AddHist(deltaEtaHT, "DeltaEta_WCR_OS_3" + RecoDecayTag);

			Region* regionWCRSS_1 = addRegion([&,i] {return (WCR && tauIDcut() && one_pron && !isOS && RecoDecayMode == i);});
			regionWCRSS_1->AddHist(upsilonHT, "Upsilon_WCR_SS_1" + RecoDecayTag);
			regionWCRSS_1->AddHist(NTrackHT, "NTrack_WCR_SS_1" + RecoDecayTag);
			regionWCRSS_1->AddHist(deltaEtaHT, "DeltaEta_WCR_SS_1" + RecoDecayTag);

			Region* regionWCRSS_3 = addRegion([&,i] {return (WCR && tauIDcut() && three_pron && !isOS && RecoDecayMode == i);});
			regionWCRSS_3->AddHist(upsilonHT, "Upsilon_WCR_SS_3" + RecoDecayTag);
			regionWCRSS_3->AddHist(NTrackHT, "NTrack_WCR_SS_3" + RecoDecayTag);
			regionWCRSS_3->AddHist(deltaEtaHT, "DeltaEta_WCR_SS_3" + RecoDecayTag);

			Region* regionMCROS_1 = addRegion([&,i] {return (MCR && tauIDcut() && one_pron && isOS && RecoDecayMode == i);});
			regionMCROS_1->AddHist(upsilonHT, "Upsilon_MCR_OS_1" + RecoDecayTag);
			regionMCROS_1->AddHist(NTrackHT, "NTrack_MCR_OS_1" + RecoDecayTag);
			regionMCROS_1->AddHist(deltaEtaHT, "DeltaEta_MCR_OS_1" + RecoDecayTag);

			Region* regionMCROS_3 = addRegion([&,i] {return (MCR && tauIDcut() && three_pron && isOS && RecoDecayMode == i);});
			regionMCROS_3->AddHist(upsilonHT, "Upsilon_MCR_OS_3" + RecoDecayTag);
			regionMCROS_3->AddHist(NTrackHT, "NTrack_MCR_OS_3" + RecoDecayTag);
			regionMCROS_3->AddHist(deltaEtaHT, "DeltaEta_MCR_OS_3" + RecoDecayTag);

			Region* regionMCRSS_1 = addRegion([&,i] {return (MCR && tauIDcut() && one_pron && !isOS && RecoDecayMode == i);});
			regionMCRSS_1->AddHist(upsilonHT, "Upsilon_MCR_SS_1" + RecoDecayTag);
			regionMCRSS_1->AddHist(NTrackHT, "NTrack_MCR_SS_1" + RecoDecayTag);
			regionMCRSS_1->AddHist(deltaEtaHT, "DeltaEta_MCR_SS_1" + RecoDecayTag);

			Region* regionMCRSS_3 = addRegion([&,i] {return (MCR && tauIDcut() && three_pron && !isOS && RecoDecayMode == i);});
			regionMCRSS_3->AddHist(upsilonHT, "Upsilon_MCR_SS_3" + RecoDecayTag);
			regionMCRSS_3->AddHist(NTrackHT, "NTrack_MCR_SS_3" + RecoDecayTag);
			regionMCRSS_3->AddHist(deltaEtaHT, "DeltaEta_MCR_SS_3" + RecoDecayTag);
		}
	}
};

//for signal tau, we need to add the information of truthX -> recoY. Besides, the sysmatic of TES and TER should be added.
class signalTau : public tau{
public:
	signalTau(std::string outfilename = "output.root") :tau(outfilename) {};
	std::function<double()> upsilon_TESup = [&] {return ((2 * tauAlltracked_pt - (1 + 0.05) * tauReco_pt) / ((1 + 0.05) * tauReco_pt));};
	std::function<double()> upsilon_TESdown = [&] {return ((2 * tauAlltracked_pt - (1 - 0.05) * tauReco_pt) / ((1 - 0.05) * tauReco_pt));};
	std::function<double()> upsilon_TERup = 
		[&] {return ((2 * tauAlltracked_pt - (tauReco_pt + (0.05)*fabs(tauReco_pt - tauTruth_pt))) / (tauReco_pt + (0.05)*fabs(tauReco_pt - tauTruth_pt)));};
	std::function<double()> upsilon_TERdown =
		[&] {return ((2 * tauAlltracked_pt - (tauReco_pt - (0.05)*fabs(tauReco_pt - tauTruth_pt))) / (tauReco_pt - (0.05)*fabs(tauReco_pt - tauTruth_pt)));};
	void registerExtraRegions() {

		//loop for different Reco decaymode
		std::string TruDecayTag,RecoDecayTag;

		for (int j = 0; j < 5; ++j) {
			switch (j)
			{
			case 0: TruDecayTag = "_Tru1"; break;
			case 1: TruDecayTag = "_Tru2"; break;
			case 2: TruDecayTag = "_Tru3"; break;
			case 3: TruDecayTag = "_Tru4"; break;
			case 4: TruDecayTag = "_Tru5"; break;
			default:break;
			}
			for (int i = 0; i < 5; i++)
			{
				double LowValueNT(0), UpValueNT(12);
				double LowValue(-1), UpValue(1.5);
				double LowValueDE(0), UpValueDE(2.5);
				int Nbins(25), NbinsNT(12), NbinsDE(10);
				if (0 == i) {
					RecoDecayTag = "_Reco1";
					Nbins = 15;
					LowValue = 0;
					UpValue = 1.5;
					UpValueNT = 5; 
					NbinsNT = 5; 
				}else if (1 == i) {
					RecoDecayTag = "_Reco2";
					Nbins = 15;
					LowValue = -1;
					UpValue = 1.5;
				}else if (2 == i) {
					RecoDecayTag = "_Reco3";
					Nbins = 15;
					LowValue = -1;
					UpValue = 0.5;
				}else if (3 == i) {
					RecoDecayTag = "_Reco4";
					Nbins = 10;
					LowValue = 0.5;
					UpValue = 1.5;
				}else if (4 == i) {
					RecoDecayTag = "_Reco5";
					Nbins = 15;
					LowValue = 0;
					UpValue = 1.5;
				}

				HistTemplate upsilonHT(upsilon, Nbins, LowValue, UpValue);
				HistTemplate upsilonTESupHT(upsilon_TESup, Nbins, LowValue, UpValue);
				HistTemplate upsilonTESdownHT(upsilon_TESdown, Nbins, LowValue, UpValue);
				HistTemplate upsilonTERupHT(upsilon_TERup, Nbins, LowValue, UpValue);
				HistTemplate upsilonTERdownHT(upsilon_TERdown, Nbins, LowValue, UpValue);
				HistTemplate NTrackHT(NTrack, NbinsNT, LowValueNT, UpValueNT);
				HistTemplate deltaEtaHT(deltaEta, NbinsDE, LowValueDE, UpValueDE);
	
				Region* regionSR = addRegion([&,i,j] {return (SR && tauIDcut() && RecoDecayMode == i && TruDecayMode == j);});
				regionSR->AddHist(upsilonHT, "Upsilon_SR" + TruDecayTag + RecoDecayTag);
				regionSR->AddHist(NTrackHT, "NTrack_SR" + TruDecayTag + RecoDecayTag);
				regionSR->AddHist(deltaEtaHT, "DeltaEta_SR" + TruDecayTag + RecoDecayTag);
				regionSR->AddHist(upsilonTESupHT, "TESup_upsilon_SR" + TruDecayTag + RecoDecayTag);
				regionSR->AddHist(upsilonTESdownHT, "TESdown_upsilon_SR" + TruDecayTag + RecoDecayTag);
				regionSR->AddHist(upsilonTERupHT, "TERup_upsilon_SR" + TruDecayTag + RecoDecayTag);
				regionSR->AddHist(upsilonTERdownHT, "TERdown_upsilon_SR" + TruDecayTag + RecoDecayTag);

				Region* regionSR_1 = addRegion([&,i,j] {return (SR && tauIDcut() && one_pron && RecoDecayMode == i && TruDecayMode == j);});
				regionSR_1->AddHist(upsilonHT, "Upsilon_SR_1" + TruDecayTag + RecoDecayTag);
				regionSR_1->AddHist(NTrackHT, "NTrack_SR_1" + TruDecayTag + RecoDecayTag);
				regionSR_1->AddHist(deltaEtaHT, "DeltaEta_SR_1" + TruDecayTag + RecoDecayTag);
				regionSR_1->AddHist(upsilonTESupHT, "TESup_upsilon_SR_1" + TruDecayTag + RecoDecayTag);
				regionSR_1->AddHist(upsilonTESdownHT, "TESdown_upsilon_SR_1" + TruDecayTag + RecoDecayTag);
				regionSR_1->AddHist(upsilonTERupHT, "TERup_upsilon_SR_1" + TruDecayTag + RecoDecayTag);
				regionSR_1->AddHist(upsilonTERdownHT, "TERdown_upsilon_SR_1" + TruDecayTag + RecoDecayTag);

				Region* regionSR_3 = addRegion([&,i,j] {return (SR && tauIDcut() && three_pron && RecoDecayMode == i && TruDecayMode == j);});
				regionSR_3->AddHist(upsilonHT, "Upsilon_SR_3" + TruDecayTag + RecoDecayTag);
				regionSR_3->AddHist(NTrackHT, "NTrack_SR_3" + TruDecayTag + RecoDecayTag);
				regionSR_3->AddHist(deltaEtaHT, "DeltaEta_SR_3" + TruDecayTag + RecoDecayTag);
				regionSR_3->AddHist(upsilonTESupHT, "TESup_upsilon_SR_3" + TruDecayTag + RecoDecayTag);
				regionSR_3->AddHist(upsilonTESdownHT, "TESdown_upsilon_SR_3" + TruDecayTag + RecoDecayTag);
				regionSR_3->AddHist(upsilonTERupHT, "TERup_upsilon_SR_3" + TruDecayTag + RecoDecayTag);
				regionSR_3->AddHist(upsilonTERdownHT, "TERdown_upsilon_SR_3" + TruDecayTag + RecoDecayTag);

				Region* regionSROS = addRegion([&,i,j] {return SR && tauIDcut() && isOS && RecoDecayMode == i && TruDecayMode == j;});
				regionSROS->AddHist(upsilonHT, "Upsilon_SR_OS" + TruDecayTag + RecoDecayTag);
				regionSROS->AddHist(NTrackHT, "NTrack_SR_OS" + TruDecayTag + RecoDecayTag);
				regionSROS->AddHist(deltaEtaHT, "DeltaEta_SR_OS" + TruDecayTag + RecoDecayTag);
				regionSROS->AddHist(upsilonTESupHT, "TESup_upsilon_SR_OS" + TruDecayTag + RecoDecayTag);
				regionSROS->AddHist(upsilonTESdownHT, "TESdown_upsilon_SR_OS" + TruDecayTag + RecoDecayTag);
				regionSROS->AddHist(upsilonTERupHT, "TERup_upsilon_SR_OS" + TruDecayTag + RecoDecayTag);
				regionSROS->AddHist(upsilonTERdownHT, "TERdown_upsilon_SR_OS" + TruDecayTag + RecoDecayTag);

				Region* regionSROS_1 = addRegion([&,i,j] {return (SR && tauIDcut() && isOS && one_pron && RecoDecayMode == i && TruDecayMode == j);});
				regionSROS_1->AddHist(upsilonHT, "Upsilon_SR_OS_1" + TruDecayTag + RecoDecayTag);
				regionSROS_1->AddHist(NTrackHT, "NTrack_SR_OS_1" + TruDecayTag + RecoDecayTag);
				regionSROS_1->AddHist(deltaEtaHT, "DeltaEta_SR_OS_1" + TruDecayTag + RecoDecayTag);
				regionSROS_1->AddHist(upsilonTESupHT, "TESup_upsilon_SR_OS_1" + TruDecayTag + RecoDecayTag);
				regionSROS_1->AddHist(upsilonTESdownHT, "TESdown_upsilon_SR_OS_1" + TruDecayTag + RecoDecayTag);
				regionSROS_1->AddHist(upsilonTERupHT, "TERup_upsilon_SR_OS_1" + TruDecayTag + RecoDecayTag);
				regionSROS_1->AddHist(upsilonTERdownHT, "TERdown_upsilon_SR_OS_1" + TruDecayTag + RecoDecayTag);

				Region* regionSROS_3 = addRegion([&,i,j] {return (SR && tauIDcut() && isOS && three_pron && RecoDecayMode == i && TruDecayMode == j);});
				regionSROS_3->AddHist(upsilonHT, "Upsilon_SR_OS_3" + TruDecayTag + RecoDecayTag);
				regionSROS_3->AddHist(NTrackHT, "NTrack_SR_OS_3" + TruDecayTag + RecoDecayTag);
				regionSROS_3->AddHist(deltaEtaHT, "DeltaEta_SR_OS_3" + TruDecayTag + RecoDecayTag);
				regionSROS_3->AddHist(upsilonTESupHT, "TESup_upsilon_SR_OS_3" + TruDecayTag + RecoDecayTag);
				regionSROS_3->AddHist(upsilonTESdownHT, "TESdown_upsilon_SR_OS_3" + TruDecayTag + RecoDecayTag);
				regionSROS_3->AddHist(upsilonTERupHT, "TERup_upsilon_SR_OS_3" + TruDecayTag + RecoDecayTag);
				regionSROS_3->AddHist(upsilonTERdownHT, "TERdown_upsilon_SR_OS_3" + TruDecayTag + RecoDecayTag);

				Region* regionSRSS = addRegion([&,i,j] {return (SR && tauIDcut() && !isOS && RecoDecayMode == i && TruDecayMode == j);});
				regionSRSS->AddHist(upsilonHT, "Upsilon_SR_SS" + TruDecayTag + RecoDecayTag);
				regionSRSS->AddHist(NTrackHT, "NTrack_SR_SS" + TruDecayTag + RecoDecayTag);
				regionSRSS->AddHist(deltaEtaHT, "DeltaEta_SR_SS" + TruDecayTag + RecoDecayTag);
				regionSRSS->AddHist(upsilonTESupHT, "TESup_upsilon_SR_SS" + TruDecayTag + RecoDecayTag);
				regionSRSS->AddHist(upsilonTESdownHT, "TESdown_upsilon_SR_SS" + TruDecayTag + RecoDecayTag);
				regionSRSS->AddHist(upsilonTERupHT, "TERup_upsilon_SR_SS" + TruDecayTag + RecoDecayTag);
				regionSRSS->AddHist(upsilonTERdownHT, "TERdown_upsilon_SR_SS" + TruDecayTag + RecoDecayTag);

				Region* regionSRSS_1 = addRegion([&,i,j] {return (SR && tauIDcut() && !isOS && one_pron && RecoDecayMode == i && TruDecayMode == j);});
				regionSRSS_1->AddHist(upsilonHT, "Upsilon_SR_SS_1" + TruDecayTag + RecoDecayTag);
				regionSRSS_1->AddHist(NTrackHT, "NTrack_SR_SS_1" + TruDecayTag + RecoDecayTag);
				regionSRSS_1->AddHist(deltaEtaHT, "DeltaEta_SR_SS_1" + TruDecayTag + RecoDecayTag);
				regionSRSS_1->AddHist(upsilonTESupHT, "TESup_upsilon_SR_SS_1" + TruDecayTag + RecoDecayTag);
				regionSRSS_1->AddHist(upsilonTESdownHT, "TESdown_upsilon_SR_SS_1" + TruDecayTag + RecoDecayTag);
				regionSRSS_1->AddHist(upsilonTERupHT, "TERup_upsilon_SR_SS_1" + TruDecayTag + RecoDecayTag);
				regionSRSS_1->AddHist(upsilonTERdownHT, "TERdown_upsilon_SR_SS_1" + TruDecayTag + RecoDecayTag);

				Region* regionSRSS_3 = addRegion([&,i,j] {return (SR && tauIDcut() && !isOS && three_pron && RecoDecayMode == i && TruDecayMode == j);});
				regionSRSS_3->AddHist(upsilonHT, "Upsilon_SR_SS_3" + TruDecayTag + RecoDecayTag);
				regionSRSS_3->AddHist(NTrackHT, "NTrack_SR_SS_3" + TruDecayTag + RecoDecayTag);
				regionSRSS_3->AddHist(deltaEtaHT, "DeltaEta_SR_SS_3" + TruDecayTag + RecoDecayTag);
				regionSRSS_3->AddHist(upsilonTESupHT, "TESup_upsilon_SR_SS_3" + TruDecayTag + RecoDecayTag);
				regionSRSS_3->AddHist(upsilonTESdownHT, "TESdown_upsilon_SR_SS_3" + TruDecayTag + RecoDecayTag);
				regionSRSS_3->AddHist(upsilonTERupHT, "TERup_upsilon_SR_SS_3" + TruDecayTag + RecoDecayTag);
				regionSRSS_3->AddHist(upsilonTERdownHT, "TERdown_upsilon_SR_SS_3" + TruDecayTag + RecoDecayTag);

				Region* region_lep_true = addRegion([&,i,j] {return (SR && tauIDcut() && isOS && isTrueTau && RecoDecayMode == i && TruDecayMode == j);});
				region_lep_true->AddHist(upsilonHT, "Upsilon_lep_true" + TruDecayTag + RecoDecayTag);
				region_lep_true->AddHist(NTrackHT, "NTrack_lep_true" + TruDecayTag + RecoDecayTag);
				region_lep_true->AddHist(deltaEtaHT, "DeltaEta_lep_true" + TruDecayTag + RecoDecayTag);
				region_lep_true->AddHist(upsilonTESupHT, "TESup_upsilon_lep_true" + TruDecayTag + RecoDecayTag);
				region_lep_true->AddHist(upsilonTESdownHT, "TESdown_upsilon_lep_true" + TruDecayTag + RecoDecayTag);
				region_lep_true->AddHist(upsilonTERupHT, "TERup_upsilon_lep_true" + TruDecayTag + RecoDecayTag);
				region_lep_true->AddHist(upsilonTERdownHT, "TERdown_upsilon_lep_true" + TruDecayTag + RecoDecayTag);

				Region* region_lep_true_1 = addRegion([&,i,j] {return (SR && tauIDcut() && isOS && isTrueTau && one_pron && RecoDecayMode == i && TruDecayMode == j);});
				region_lep_true_1->AddHist(upsilonHT, "Upsilon_lep_true_1" + TruDecayTag + RecoDecayTag);
				region_lep_true_1->AddHist(NTrackHT, "NTrack_lep_true_1" + TruDecayTag + RecoDecayTag);
				region_lep_true_1->AddHist(deltaEtaHT, "DeltaEta_lep_true_1" + TruDecayTag + RecoDecayTag);
				region_lep_true_1->AddHist(upsilonTESupHT, "TESup_upsilon_lep_true_1" + TruDecayTag + RecoDecayTag);
				region_lep_true_1->AddHist(upsilonTESdownHT, "TESdown_upsilon_lep_true_1" + TruDecayTag + RecoDecayTag);
				region_lep_true_1->AddHist(upsilonTERupHT, "TERup_upsilon_lep_true_1" + TruDecayTag + RecoDecayTag);
				region_lep_true_1->AddHist(upsilonTERdownHT, "TERdown_upsilon_lep_true_1" + TruDecayTag + RecoDecayTag);

				Region* region_lep_true_3 = addRegion([&,i,j] {return (SR && tauIDcut() && isOS && isTrueTau && three_pron && RecoDecayMode == i && TruDecayMode == j);});
				region_lep_true_3->AddHist(upsilonHT, "Upsilon_lep_true_3" + TruDecayTag + RecoDecayTag);
				region_lep_true_3->AddHist(NTrackHT, "NTrack_lep_true_3" + TruDecayTag + RecoDecayTag);
				region_lep_true_3->AddHist(deltaEtaHT, "DeltaEta_lep_true_3" + TruDecayTag + RecoDecayTag);
				region_lep_true_3->AddHist(upsilonTESupHT, "TESup_upsilon_lep_true_3" + TruDecayTag + RecoDecayTag);
				region_lep_true_3->AddHist(upsilonTESdownHT, "TESdown_upsilon_lep_true_3" + TruDecayTag + RecoDecayTag);
				region_lep_true_3->AddHist(upsilonTERupHT, "TERup_upsilon_lep_true_3" + TruDecayTag + RecoDecayTag);
				region_lep_true_3->AddHist(upsilonTERdownHT, "TERdown_upsilon_lep_true_3" + TruDecayTag + RecoDecayTag);

				Region* region_lep_fake = addRegion([&,i,j] {return (SR && tauIDcut() && isOS && isLepFake && RecoDecayMode == i && TruDecayMode == j);});
				region_lep_fake->AddHist(upsilonHT, "Upsilon_lep_fake" + TruDecayTag + RecoDecayTag);
				region_lep_fake->AddHist(NTrackHT, "NTrack_lep_fake" + TruDecayTag + RecoDecayTag);
				region_lep_fake->AddHist(deltaEtaHT, "DeltaEta_lep_fake" + TruDecayTag + RecoDecayTag);
				region_lep_fake->AddHist(upsilonTESupHT, "TESup_upsilon_lep_fake" + TruDecayTag + RecoDecayTag);
				region_lep_fake->AddHist(upsilonTESdownHT, "TESdown_upsilon_lep_fake" + TruDecayTag + RecoDecayTag);
				region_lep_fake->AddHist(upsilonTERupHT, "TERup_upsilon_lep_fake" + TruDecayTag + RecoDecayTag);
				region_lep_fake->AddHist(upsilonTERdownHT, "TERdown_upsilon_lep_fake" + TruDecayTag + RecoDecayTag);

				Region* region_lep_fake_1 = addRegion([&,i,j] {return (SR && tauIDcut() && isOS && isLepFake && one_pron && RecoDecayMode == i && TruDecayMode == j);});
				region_lep_fake_1->AddHist(upsilonHT, "Upsilon_lep_fake_1" + TruDecayTag + RecoDecayTag);
				region_lep_fake_1->AddHist(NTrackHT, "NTrack_lep_fake_1" + TruDecayTag + RecoDecayTag);
				region_lep_fake_1->AddHist(deltaEtaHT, "DeltaEta_lep_fake_1" + TruDecayTag + RecoDecayTag);
				region_lep_fake_1->AddHist(upsilonTESupHT, "TESup_upsilon_lep_fake_1" + TruDecayTag + RecoDecayTag);
				region_lep_fake_1->AddHist(upsilonTESdownHT, "TESdown_upsilon_lep_fake_1" + TruDecayTag + RecoDecayTag);
				region_lep_fake_1->AddHist(upsilonTERupHT, "TERup_upsilon_lep_fake_1" + TruDecayTag + RecoDecayTag);
				region_lep_fake_1->AddHist(upsilonTERdownHT, "TERdown_upsilon_lep_fake_1" + TruDecayTag + RecoDecayTag);

				Region* region_lep_fake_3 = addRegion([&,i,j] {return (SR && tauIDcut() && isOS && isLepFake && three_pron && RecoDecayMode == i && TruDecayMode == j);});
				region_lep_fake_3->AddHist(upsilonHT, "Upsilon_lep_fake_3" + TruDecayTag + RecoDecayTag);
				region_lep_fake_3->AddHist(NTrackHT, "NTrack_lep_fake_3" + TruDecayTag + RecoDecayTag);
				region_lep_fake_3->AddHist(deltaEtaHT, "DeltaEta_lep_fake_3" + TruDecayTag + RecoDecayTag);
				region_lep_fake_3->AddHist(upsilonTESupHT, "TESup_upsilon_lep_fake_3" + TruDecayTag + RecoDecayTag);
				region_lep_fake_3->AddHist(upsilonTESdownHT, "TESdown_upsilon_lep_fake_3" + TruDecayTag + RecoDecayTag);
				region_lep_fake_3->AddHist(upsilonTERupHT, "TERup_upsilon_lep_fake_3" + TruDecayTag + RecoDecayTag);
				region_lep_fake_3->AddHist(upsilonTERdownHT, "TERdown_upsilon_lep_fake_3" + TruDecayTag + RecoDecayTag);

				Region* regionWCROS = addRegion([&,i,j] {return (WCR && tauIDcut() && isOS && RecoDecayMode == i && TruDecayMode == j);});
				regionWCROS->AddHist(upsilonHT, "Upsilon_WCR_OS" + TruDecayTag + RecoDecayTag);
				regionWCROS->AddHist(NTrackHT, "NTrack_WCR_OS" + TruDecayTag + RecoDecayTag);
				regionWCROS->AddHist(deltaEtaHT, "DeltaEta_WCR_OS" + TruDecayTag + RecoDecayTag);
				regionWCROS->AddHist(upsilonTESupHT, "TESup_upsilon_WCR_OS" + TruDecayTag + RecoDecayTag);
				regionWCROS->AddHist(upsilonTESdownHT, "TESdown_upsilon_WCR_OS" + TruDecayTag + RecoDecayTag);
				regionWCROS->AddHist(upsilonTERupHT, "TERup_upsilon_WCR_OS" + TruDecayTag + RecoDecayTag);
				regionWCROS->AddHist(upsilonTERdownHT, "TERdown_upsilon_WCR_OS" + TruDecayTag + RecoDecayTag);

				Region* regionWCRSS = addRegion([&,i,j] {return (WCR && tauIDcut() && !isOS && RecoDecayMode == i && TruDecayMode == j);});
				regionWCRSS->AddHist(upsilonHT, "Upsilon_WCR_SS" + TruDecayTag + RecoDecayTag);
				regionWCRSS->AddHist(NTrackHT, "NTrack_WCR_SS" + TruDecayTag + RecoDecayTag);
				regionWCRSS->AddHist(deltaEtaHT, "DeltaEta_WCR_SS" + TruDecayTag + RecoDecayTag);
				regionWCRSS->AddHist(upsilonTESupHT, "TESup_upsilon_WCR_SS" + TruDecayTag + RecoDecayTag);
				regionWCRSS->AddHist(upsilonTESdownHT, "TESdown_upsilon_WCR_SS" + TruDecayTag + RecoDecayTag);
				regionWCRSS->AddHist(upsilonTERupHT, "TERup_upsilon_WCR_SS" + TruDecayTag + RecoDecayTag);
				regionWCRSS->AddHist(upsilonTERdownHT, "TERdown_upsilon_WCR_SS" + TruDecayTag + RecoDecayTag);

				Region* regionMCROS = addRegion([&,i,j] {return (MCR && tauIDcut() && isOS && RecoDecayMode == i && TruDecayMode == j);});
				regionMCROS->AddHist(upsilonHT, "Upsilon_MCR_OS" + TruDecayTag + RecoDecayTag);
				regionMCROS->AddHist(NTrackHT, "NTrack_MCR_OS" + TruDecayTag + RecoDecayTag);
				regionMCROS->AddHist(deltaEtaHT, "DeltaEta_MCR_OS" + TruDecayTag + RecoDecayTag);
				regionMCROS->AddHist(upsilonTESupHT, "TESup_upsilon_MCR_OS" + TruDecayTag + RecoDecayTag);
				regionMCROS->AddHist(upsilonTESdownHT, "TESdown_upsilon_MCR_OS" + TruDecayTag + RecoDecayTag);
				regionMCROS->AddHist(upsilonTERupHT, "TERup_upsilon_MCR_OS" + TruDecayTag + RecoDecayTag);
				regionMCROS->AddHist(upsilonTERdownHT, "TERdown_upsilon_MCR_OS" + TruDecayTag + RecoDecayTag);

				Region* regionMCRSS = addRegion([&,i,j] {return (MCR && tauIDcut() && !isOS && RecoDecayMode == i && TruDecayMode == j);});
				regionMCRSS->AddHist(upsilonHT, "Upsilon_MCR_SS" + TruDecayTag + RecoDecayTag);
				regionMCRSS->AddHist(NTrackHT, "NTrack_MCR_SS" + TruDecayTag + RecoDecayTag);
				regionMCRSS->AddHist(deltaEtaHT, "DeltaEta_MCR_SS" + TruDecayTag + RecoDecayTag);
				regionMCRSS->AddHist(upsilonTESupHT, "TESup_upsilon_MCR_SS" + TruDecayTag + RecoDecayTag);
				regionMCRSS->AddHist(upsilonTESdownHT, "TESdown_upsilon_MCR_SS" + TruDecayTag + RecoDecayTag);
				regionMCRSS->AddHist(upsilonTERupHT, "TERup_upsilon_MCR_SS" + TruDecayTag + RecoDecayTag);
				regionMCRSS->AddHist(upsilonTERdownHT, "TERdown_upsilon_MCR_SS" + TruDecayTag + RecoDecayTag);
			}
		}
	}
};

#endif
