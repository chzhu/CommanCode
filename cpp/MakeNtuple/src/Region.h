#pragma once
#include <TH1.h>
#include <functional>

// 1D Histgram with its fill position 
class Hist
{
public:

	TH1 * hist;
	std::function<double()> fillPosition;

	Hist(TH1* hist, std::function<double()> fillPosition) {
		hist->Sumw2();
		this->hist = hist;
		this->fillPosition = fillPosition;
	}
	//In this way, you can only use TH1F
	Hist(std::string histname, int nBin, double binStart, double binEnd, std::function<double()> fillPosition) {
		TH1* hist = new TH1F(histname.c_str(), histname.c_str(), nBin, binStart, binEnd);
		hist->Sumw2();
		this->hist = hist;
		this->fillPosition = fillPosition;
	}
	void Fill(double weight) {
		hist->Fill(fillPosition(),weight);
	}
	void Write(){
		hist->Write();
	}
	~Hist(){};
};

//A class that allow you to create many Hist with same bin definition and fill Position
class HistTemplate
{

public:

	HistTemplate(std::function<double()> fillValue, int binNum, double binStart, double binEnd) {
		this->fillValue = fillValue;
		this->binNum = binNum;
		this->binStart = binStart;
		this->binEnd = binEnd;
	}
	Hist* createHist(std::string name){
		return new Hist(name, binNum, binStart, binEnd, fillValue);
	}
	~HistTemplate() {};

private:
	std::function<double()> fillValue;
	int binNum;
	double binStart, binEnd;

};


//A region is defined by cuts. We can also register some histograms into it
class Region {
public:

	std::function<bool()> cut;
	std::map<TH1*, std::function<double()>> hists;

	Region(std::function<bool()> cut) {
		this->cut = cut;
	};
	Region() {}
	void setCut(std::function<bool()> cut) {
		this->cut = cut;
	}
	void AddHist(std::function<double()> value, std::string name, int Nbins, double LowValue, double UpValue) {
		TH1* hist = new TH1F(name.c_str(), name.c_str(), Nbins, LowValue, UpValue);
		hist->Sumw2();
		hists[hist] = value;
	}
	void AddHist(std::function<double()> value, TH1* hist) {
		hist->Sumw2();
		hists[hist] = value;
	}
	void AddHist(HistTemplate histtemplate, std::string name) {
		Hist* histWithFillPosi = histtemplate.createHist(name);
		histogram = histWithFillPosi->hist;
		hists[hist] = histWithFillPosi->fillPosition;
	}
	void fillHists(double weight) {
		if (cut())
		{
			std::map<TH1*, std::function<double()>>::iterator iter;
			for (iter = hists.begin(); iter != hists.end(); iter++) {
				iter->first->Fill(iter->second(), weight);
			}
		}
	}
};
