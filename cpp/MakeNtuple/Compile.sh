#------------important! boost environment setting---------
#Have some error while using gcc62 version boost. Temprory use gcc49 version boost and root
gccCheck=`gcc --version | grep 4.9`
if [ ! $gccCheck ]; then
	lsetup "root 6.08.02-x86_64-slc6-gcc49-opt"
fi

#not used, keep just in case
#export PATH=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/Gcc/gcc493_x86_64_slc6/slc6/gcc49/bin:/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.04.16-x86_64-slc6-gcc49-opt/bin:$PATH

#------------------------compile-----------------------
if [ ! -d obj ]; then
		mkdir obj
fi

make clean
make
