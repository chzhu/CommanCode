#pragma once 
// there we store the meata data

/**********************************This is for AF2 ***************************************/
double GetRunNgenAF2(UInt_t run) {
	if (run == 361108) return    74144546559.657959;
	else return 0;
}

/**********************************This is for R21 ***************************************/
double GetRunNgenR21( UInt_t run ){
	if (run == 361100 ) 	 return    462609993702.310547;     
	else if (run == 361101 ) return    454487262987.404297;
	else if (run == 361102 ) return    338749068691.189453;
	else if (run == 361103 ) return    413857683381.431641;
	else if (run == 361104 ) return    264876907104.890625;
	else if (run == 361105 ) return    165398730373.500000;
	else if (run == 361106 ) return    150831082759.631348;
	else if (run == 361107 ) return    115784723287.483887;
	else if (run == 361108 ) return    74007574037.058105;
	else if (run == 410000 ) return    4.91466000000000000e+07;
	else if (run == 410501 ) return    14601556412.838745;
	else return 0;
}


/**********************************This is for R21 (V2 version)***************************************
double GetRunNgenR21( UInt_t run ){
	if (run == 361100 ) 	 return    474464794804.113281;     
	else if (run == 361101 ) return    451808282784.146484;
	else if (run == 361102 ) return    338975727157.652344;
	else if (run == 361103 ) return    413338391815.402344;
	else if (run == 361104 ) return    264816513116.099609;
	else if (run == 361105 ) return    165275371991.951172;
	else if (run == 361106 ) return    151981763730.859131;
	else if (run == 361107 ) return    151851897976.563965;
	else if (run == 361108 ) return    50075331191.061035;
	else if (run == 410000 ) return    4.91466000000000000e+07;
	else if (run == 410501 ) return    43768403026.539795;
	else return 0;
}
******************************************************************************************/



double GetXsecR21(UInt_t run){
	if (run == 361100 ) 	 return  11306. ;     
	else if (run == 361101 ) return  11306. ;
	else if (run == 361102 ) return  11306. ;
	else if (run == 361103 ) return  8282.7;
	else if (run == 361104 ) return  8282.8;
	else if (run == 361105 ) return  8282.6;
	else if (run == 361106 ) return  1901.2;
	else if (run == 361107 ) return  1901.1;
	else if (run == 361108 ) return  1901.2;
	else if (run == 410000 ) return   696.11;
	else if (run == 410501 ) return   730.170;
	else return 0;
}


double GetKFactorR21(UInt_t run){
	if (run == 361100 ) 	 return  1.01723999646 ;     
	else if (run == 361101 ) return  1.01723999646 ;
	else if (run == 361102 ) return  1.01723999646 ;
	else if (run == 361103 ) return  1.03577349174;
	else if (run == 361104 ) return  1.03576098662;
	else if (run == 361105 ) return  1.03578599715;
	else if (run == 361106 ) return  1.02600047339;
	else if (run == 361107 ) return  1.02605444217;
	else if (run == 361108 ) return  1.02605444217;
	else if (run == 410000 ) return   1.1949;
	else if (run == 410501 ) return   1.13913198296;
	else return 0;
}


double GetFilterEffR21(UInt_t run){
	if (run == 361100 ) 	 return  1. ;     
	else if (run == 361101 ) return  1. ;
	else if (run == 361102 ) return  1. ;
	else if (run == 361103 ) return   1.;
	else if (run == 361104 ) return   1.;
	else if (run == 361105 ) return   1.;
	else if (run == 361106 ) return   1.;
	else if (run == 361107 ) return   1.;
	else if (run == 361108 ) return   1.;
	else if (run == 410000 ) return   0.543;
	else if (run == 410501 ) return   0.54383;
	else return 0;
}

/**********************************This is for R20 ***************************************/

double GetRunNgenR20( UInt_t run ){
	if (run == 361100 ) return    3.37809927498035156e+11 ;     
	else if (run == 361101 ) return    2.60339894551261719e+11 ;
	else if (run == 361102 ) return    3.37342173712105469e+11 ;
	else if (run == 361103 ) return    1.64845297900125000e+11;
	else if (run == 361104 ) return    1.26532487359693359e+11 ;
	else if (run == 361105 ) return    1.65245467148560547e+11 ;
	else if (run == 361106 ) return    1.50277594199963501e+11;
	else if (run == 361107 ) return    1.38896943875657227e+11;
	else if (run == 361108 ) return    4.29392375100476074e+10;
	else if (run == 410000 ) return    4.91466000000000000e+07;
	else return 0;
}



double GetXsecR20(UInt_t run){
	if (run == 361100 ) 	 return  11306. ;     
	else if (run == 361101 ) return  11306. ;
	else if (run == 361102 ) return  11306. ;
	else if (run == 361103 ) return   8282.6;
	else if (run == 361104 ) return   8282.6;
	else if (run == 361105 ) return   8282.6;
	else if (run == 361106 ) return   1901.2;
	else if (run == 361107 ) return   1901.2;
	else if (run == 361108 ) return   1901.2;
	else if (run == 410000 ) return   696.11;
	else return 0;
}


double GetKFactorR20(UInt_t run){
	if (run == 361100 ) 	 return  1.01724 ;     
	else if (run == 361101 ) return  1.01724 ;
	else if (run == 361102 ) return  1.01724 ;
	else if (run == 361103 ) return   1.035786;
	else if (run == 361104 ) return   1.035786;
	else if (run == 361105 ) return   1.035786;
	else if (run == 361106 ) return   1.026;
	else if (run == 361107 ) return   1.026;
	else if (run == 361108 ) return   1.026;
	else if (run == 410000 ) return   1.1949;
	else return 0;
}

double GetFilterEffR20(UInt_t run){
	if (run == 361100 ) 	 return  1. ;     
	else if (run == 361101 ) return  1. ;
	else if (run == 361102 ) return  1. ;
	else if (run == 361103 ) return   1.;
	else if (run == 361104 ) return   1.;
	else if (run == 361105 ) return   1.;
	else if (run == 361106 ) return   1.;
	else if (run == 361107 ) return   1.;
	else if (run == 361108 ) return   1.;
	else if (run == 410000 ) return   0.543;
	else if (run == 410501 ) return   0.54383;
	else return 0;
}


