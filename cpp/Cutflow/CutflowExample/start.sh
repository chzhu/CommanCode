path1="../../../../../../data/SubTau/"
path2="/publicfs/atlas/atlasnew/SUSY/users/zhucz/output/signaltest/"
path3="/publicfs/atlas/atlasnew/SUSY/users/zhucz/output/signal/"
#files1=data.root
files1=`ls ${path1}`
files2=`ls ${path2}`
files3=`ls ${path3}`

make clean
make

chmod u+x R21Tree_ana
cd recon1
rm cutflow.csv
nohup ../R21Tree_ana background 1 ../../../../../../data/SubTau/ data.root multiBkg.sh ttbar.root W.root Zll.root Ztt.root > log &
cd ../recon2
rm cutflow.csv
nohup ../R21Tree_ana background 2 ../../../../../../data/SubTau/ data.root multiBkg.sh ttbar.root W.root Zll.root Ztt.root > log &
cd ../recon3
rm cutflow.csv
nohup ../R21Tree_ana background 3 ../../../../../../data/SubTau/ data.root multiBkg.sh ttbar.root W.root Zll.root Ztt.root > log &
cd ../recon4
rm cutflow.csv
nohup ../R21Tree_ana background 4 ../../../../../../data/SubTau/ data.root multiBkg.sh ttbar.root W.root Zll.root Ztt.root > log &
cd ../recon5
rm cutflow.csv
nohup ../R21Tree_ana background 5 ../../../../../../data/SubTau/ data.root multiBkg.sh ttbar.root W.root Zll.root Ztt.root > log &
cd ../
#./R21Tree_ana background OS $path1 $files1&
#./R21Tree_ana signal OS $path2 $files2
#./R21Tree_ana signal OS $path3 $files3
