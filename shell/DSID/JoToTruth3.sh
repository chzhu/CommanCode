run_path=$1
runNumber=$2
if [ "$run_path" = "" -o ! "$runNumber" ]; then
		echo Usage: souce run.sh foldername runNumber
else
		truth_path="truth3_"$run_path
		if [ ! -d $run_path ]; then
				mkdir $run_path
		fi
		if [ ! -d $truth_path ]; then
				mkdir $truth_path
		fi

		#EVNT
		cd $run_path
		asetup AtlasProduction,19.2.5.23,here
		DSID=`expr $runNumber / 1000`
		filename=`find /cvmfs/atlas.cern.ch/repo/sw/Generators/MC15JobOptions/latest/share/DSID"$DSID"xxx/ -name "*$runNumber*.py"`
		echo $filename
		Generate_tf.py --ecmEnergy=14000. --runNumber=$runNumber --firstEvent=1 --maxEvents=-1 --randomSeed=12321 --jobConfig=$filename --outputEVNTFile=$run_path.$runNumber.pool.root
		
		#truth3
		cd ../$truth_path
		asetup 20.7.8.7,AtlasDerivation,gcc49,here
		Reco_tf.py --inputEVNTFile ../$run_path/$run_path.$runNumber.pool.root --outputDAODFile $run_path.$runNumber.pool.root --reductionConf TRUTH3
		full_path=`pwd`
		
		#Simple Analysis
		cd /afs/cern.ch/work/c/chzhu/public/framework/SimpleAnalysis
		
		#if no 'rc' command, setup it
		hash rc 2>/dev/null || source rcSetup.sh

		rc find_packages
		rc compile
		
		cd run/
		if [ ! -d $run_path ]; then
				mkdir "$run_path"15
		fi
		cd "$run_path"15
		simpleAnalysis -a EwkTwoTau2017 $full_path/DAOD_TRUTH3.$run_path.$runNumber.pool.root
		cd $full_path/../
fi
