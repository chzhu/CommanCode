path=/cvmfs/atlas.cern.ch/repo/sw/Generators/MC12JobOptions/latest/share/DSID205xxx/
#for i in {216..265}
for i in {271..294}
do
	rawfilename=`find $path -name "*205$i*.py"`
	midfilename=${rawfilename/\/cvmfs*MC12/mc12_14TeV}
	filename=${midfilename/%py/merge.DAOD_TRUTH3*}
	
	rawcontainer=`rucio list-dids $filename | grep CONTAINER`
	midcontainer=${rawcontainer/%p3135*\|/p3135}
	container=${midcontainer/#\|*mc12_14TeV/mc12_14TeV}
	midout=`rlf $container | grep "Total events"`
	out=${midout#Total events :}
	echo $container    $out
done
