i=1
for file in $*
do
	if [ $i -gt 1 ]; then
		echo "cp $1 to $file"
		cp -r $1 $file
	fi
	let i++
done

if [ $i -eq 1 ]; then
	echo "cp: missing file operand"
	echo 'Try "cp --help" for more information.'
elif [ $i -eq 2 ]; then
	echo "cp: missing destination file operand after '$1'"
	echo 'Try "cp --help" for more information.'
fi
