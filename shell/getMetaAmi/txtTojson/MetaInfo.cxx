#include "PhyUtils.h"
#include "Utils.h"
#include <math.h>
#include <regex>
#include <vector>
#include <map>
#include <fstream>
#include <TSystem.h>
#include <TMath.h>
#include <iomanip>
#include <string>
#include <sstream>
#include <algorithm>
#include <TFile.h>
#include <TROOT.h>
#include <TStyle.h>
#include <cmath>
#include <regex>
#include "json.hpp"

using namespace std;
using json = nlohmann::json;

bool isSig;
void getinfo(ifstream& file)
{
	string ip;
	//regex for data
	//301016 -> paramName = crossSection -> paramValue = 8.9764E-10 -- -> paramName = genFiltEff -> paramValue = 1.0000E+0
	const regex patternRun("mc1.*\\.(\\d+)\\.");
	const regex patternCro("crossSection -> paramValue = ([0-9E\\.\\+\\-]+)");
	const regex patternEff("genFiltEff -> paramValue = ([0-9E\\.\\+\\-]+)");
	const regex patternkFa("actor -> paramValue = ([0-9E\\.\\+\\-]+)");
	match_results<string::const_iterator> resultRun;
	match_results<string::const_iterator> resultCro;
	match_results<string::const_iterator> resultEff;
	match_results<string::const_iterator> resultkFa;
	
	map<string,double> sigxSec206;
	map<string,double> sigxSec207;
	map<string,double> bkgxSec;
	map<string,double> filterEff;
	map<string,double> kFac;

	//initialize the energy and the energy we aimed.
	string runNumber;
	double cross(0),eff(0),kFa(0);
	while (getline(file, ip))
	{
		bool validRun = regex_search(ip,resultRun,patternRun); 
		bool validCro = regex_search(ip,resultCro,patternCro); 
		bool validEff = regex_search(ip,resultEff,patternEff); 
		bool validkFa = regex_search(ip,resultkFa,patternkFa); 
		//if match the white line, will calculate the energy ratio and restart the energy collecting.
		//if match the data line, collect the energy.
		if (validRun){
			runNumber = resultRun[1];
			cout << runNumber << ":" << validRun << validCro << validEff << validkFa << endl;
			if(validCro){
				cross = Utils::sTod(resultCro[1]) * 1000;
				if(isSig){
					sigxSec206[runNumber] = cross;
					sigxSec207[runNumber] = cross;
				}else{
					bkgxSec[runNumber] = cross;
				}
			}
			if(validEff){
				eff = Utils::sTod(resultEff[1]);
				filterEff[runNumber] = eff;
			}
			if(validkFa){
				kFa = Utils::sTod(resultkFa[1]);
				kFac[runNumber] = kFa;
			}
		}	  
	}
    map<string,map<string,double>> CsonxSec;
    CsonxSec["0"] = bkgxSec;
    CsonxSec["206"] = sigxSec206;
    CsonxSec["207"] = sigxSec207;
    
    json jxsec(CsonxSec);
	json jkfac(kFac);
	json jfilt(filterEff);

	json total;
	total["xSection"] = jxsec;
	total["kFactor"] = jkfac;
	total["filterEff"] = jfilt;

	std::ofstream o("metadata.json");
	o << std::setw(4) << total << std::endl;


}

int main(int argc, char* argv[])
{
	string datamode = argv[1];
	if(datamode == "signal"){
		isSig = true;
	}else{
		isSig = false;
	}

    string file = argv[2];

	ifstream infile(file);
	if (!infile)
	{
		cout << "No such file!" << endl;
		return -1;
	}
	
	getinfo(infile);

    return 0;
}

