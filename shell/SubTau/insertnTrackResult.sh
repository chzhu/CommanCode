dirpath="/publicfs/atlas/atlasnew/SUSY/users/zhucz/framework/SubTau/SimpleFrame/nTrackFitBox/results/"
for tauID in loose medium tight
do
	for i in {1..5}
	do
		fullpath="$dirpath""$tauID"/Recon"$i"PullsAndCorreDATA
		filename=muhat_Recon"$i"_combined.txt
		#------------get value of numTau---------------------
		tauline=`grep "NormTau" $fullpath/$filename`
		numTau=`echo "$tauline" | awk '{print $4}'`
		tauup=`echo "$tauline" | awk '{print $5}'`
		taudown=`echo "$tauline" | awk '{print $6}'`
		if [ $(echo "$tauup > 0"|bc) = 1 ]; then
			numTauUp=$(awk "BEGIN{print $numTau+$tauup }")
			numTauDown=$(awk "BEGIN{print $numTau+$taudown }")
		else
			numTauUp=$(awk "BEGIN{print $numTau+$taudown }")
			numTauDown=$(awk "BEGIN{print $numTau+$tauup }")
		fi
		#------------get value of numJet---------------------
		jetline=`grep "NormJet" $fullpath/$filename`
		numJet=`echo "$jetline" | awk '{print $3}'`
		jetup=`echo "$jetline" | awk '{print $4}'`
		jetdown=`echo "$jetline" | awk '{print $5}'`
		if [ $(echo "$jetup > 0"|bc) = 1 ]; then
			numJetUp=$(awk "BEGIN{print $numJet+$jetup }")
			numJetDown=$(awk "BEGIN{print $numJet+$jetdown }")
		else
			numJetUp=$(awk "BEGIN{print $numJet+$jetdown }")
			numJetDown=$(awk "BEGIN{print $numJet+$jetup }")
		fi
		sed -i 's/.*NormFactor Name="NormTauR'"$i"'" Val=.*>/<NormFactor Name="NormTauR'"$i"'" Val="'"$numTau"'" Low="0.8" High="1.2" \/>/g' "$tauID"/*/config/DecayMode"$i"_channel.xml 
		sed -i 's/.*NormFactor Name="NormJetR'"$i"'" Val=.*>/<NormFactor Name="NormJetR'"$i"'" Val="'"$numJet"'" Low="0.8" High="1.2"  \/>/g' "$tauID"/*/config/DecayMode"$i"_channel.xml 
#		sed -i 's/.*NormFactor Name="NormJet" Val=.*>/<NormFactor Name="NormJetR'"$i"'" Val="'"$numJet"'" Low="'"$numJetDown"'" High="'"$numJetUp"'"  Const="True"\/>/g' "$tauID"/combine/FitWithHist2workspace/config/DecayMode"$i"_channel.xml 
#		sed -i 's/.*NormFactor Name="NormTau" Val=.*>/<NormFactor Name="NormTauR'"$i"'" Val="'"$numTau"'" Low="'"$numTauDown"'" High="'"$numTauUp"'"  Const="True"\/>/g' "$tauID"/combine/FitWithHist2workspace/config/DecayMode"$i"_channel.xml 
	done
done
