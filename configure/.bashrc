PS1='[\u@\h \W]\$'

#Alias
alias ll='ls -lasthF'
alias l='ls -CF'
alias la='ls -A'
alias mkdir='mkdir -pv'
alias sd='cd ..'
alias diff='diff -b'
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'

alias 1='cd'
alias 2='cd /workfs/atlas/zhucz'
alias 3='cd /scratchfs/atlas/zhucz'
alias 4='cd /publicfs/atlas/atlasnew/SUSY/users/zhucz'

alias rt='root -l'
alias rq='root -l -q'
alias rbq='root -l -b -q'

alias vi='vim -O'

#rucio alias
alias rlf='rucio list-files'
alias rld='rucio list-dids'
alias rd='rucio download'

#makesha1: usage makesha1 $A_folder_or_file_you_want_to_check $outputFile
alias makesha1='makefilesha1() { find $1 -type f -print0 | xargs -0 sha1sum > $2; }; makefilesha1'
#cp a file to all other dics
alias acp="source ~/codes/shell/allcp.sh $*"
#Convert Single tex file without "\begin{document} and "\end{document} to pdf"
alias texToPdf="source ~/codes/shell/texToPdf.sh $*"
#source a shell multiple times with different $1 in parallel
alias msource="~/codes/go/msource $*"

#sh path
export spath=~/codes/shell/

#Additional C,C++ include path
export C_INCLUDE_PATH=~/codes/cpp/MiniAnalysis/include:~/codes/cpp/MiniAnalysis/src:$C_INCLUDE_PATH
export CPLUS_INCLUDE_PATH=~/codes/cpp/MiniAnalysis/include:~/codes/cpp/MiniAnalysis/src:$CPLUS_INCLUDE_PATH

#pythonpath
export PYTHONPATH=~/codes/python/:$PYTHONPATH

#Additional C,C++ lib path
export LD_LIBRARY_PATH=~/codes/cpp/MiniAnalysis/lib:$LD_LIBRARY_PATH
export LIBRARY_PATH=~/codes/cpp/MiniAnalysis/lib:$LIBRARY_PATH

#setup go
export PATH=$PATH:/publicfs/atlas/atlasnew/SUSY/users/zhucz/program/go/bin/

#setup ATLAS
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'
setupATLAS
#root and cmake
#lsetup "root 6.18.04-x86_64-centos7-gcc8-opt"
#lsetup "root 6.10.06-x86_64-slc6-gcc62-opt"
lsetup cmake
#Old version of Root
unset ALRB_infoProc
#Setup HistFitter
alias setupHF='source /publicfs/atlas/atlasnew/SUSY/users/zhucz/framework/HistFitter/setup.sh'
#rucio 	type 'localSetupRucioClients' instead of 'lsetup rucio' to setup rucio
export RUCIO_ACCOUNT=chzhu
#unset PYTHONHOME to let the htcondor works again
unset PYTHONHOME
#hep_sub
export PATH=/afs/ihep.ac.cn/soft/common/sysgroup/hep_job/bin:$PATH
#When submit job, use SLC7
alias hep_sub='hep_sub -os SL7'
alias qj='hep_q -u zhucz'

#setup CEPC
alias setupCEPC='source /cvmfs/cepc.ihep.ac.cn/software/cepcenv/setup.sh'

#latex
export PATH=/cvmfs/sft.cern.ch/lcg/external/texlive/2016/bin/x86_64-linux:$PATH

#SLC6 Shell
alias SLC6Shell='/cvmfs/container.ihep.ac.cn/bin/hep_container shell SL6'

#The work dir of Xing the Heaven Hole
export xingkeng="/dybfs/comet/common/xingty/"
