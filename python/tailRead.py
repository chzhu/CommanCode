#!/usr/bin/env python
# -*- coding: UTF-8 -*-
import os,sys
def tailRead(fname, n):
    fsize = os.path.getsize(fname)
    f = open(fname, 'rb')
    f.seek(fsize - 1)
    cur_pos = f.tell()
    buf = b''
    while n:
        b = f.read(1)
        buf = b + buf
        cur_pos -= 1
        if cur_pos < 0: break
        f.seek(cur_pos)
        if b == '\n':
            n -= 1
    return buf

if __name__ == '__main__':
    buf = tailRead(sys.argv[1], int(sys.argv[2]))
    print (buf)
